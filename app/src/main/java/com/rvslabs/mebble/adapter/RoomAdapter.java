package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.Room;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 14/09/15.
 */
public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.RoomViewHolder> {

    List<Room> rooms;
    private Context context;

    private OnRoomClickListener listener;

    public void setListener(OnRoomClickListener listener) {
        this.listener = listener;
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.row_rooms, parent, false);

        // Return a new holder instance
        return new RoomViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {

        Room room = rooms.get(position);

        holder.textViewName.setText(room.getName());

        holder.textViewAddress.setText(room.getAddress());

        holder.textViewDistance.setText(room.getUsers() + " pessoas / " + room.getDistance() + "m de você");

        String imageUrl = room.getImageUrl();
        if (imageUrl != null && !imageUrl.equals("")) {
            Picasso.with(context).load(imageUrl).fit().centerCrop().into(holder.imageView);
        } else {
//            Picasso.with(context).load(R.drawable.local_image).fit().centerCrop().into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return rooms == null ? 0 : rooms.size();
    }


    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
        notifyDataSetChanged();
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.distanceTextView)
        TextView textViewDistance;
        @Bind(R.id.roomNameTextView)
        TextView textViewName;
        @Bind(R.id.addressTextView)
        TextView textViewAddress;
        @Bind(R.id.imageView)
        ImageView imageView;

        public RoomViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int layoutPosition = getLayoutPosition();

            Room room = rooms.get(layoutPosition);
            if (listener != null) {
                listener.onRoomClick(room, layoutPosition);
            }
        }
    }

    public interface OnRoomClickListener {
        void onRoomClick(Room room, int position);
    }


}
