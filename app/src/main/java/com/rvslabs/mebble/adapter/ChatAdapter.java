package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rvslabs on 31/07/15.
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements BottomSheetListener {

    private final MessagesDAO messagesDAO;
    private LayoutInflater inflater;
    private Context context;

    private ChatAdapterListener listener;

    private static int indexSelected;

    private List<MessageEntity> messageEntities;

    public void setMessageEntities(List<MessageEntity> messageEntities) {
        this.messageEntities = messageEntities;
        notifyItemInserted(messageEntities.size());
    }

    public ChatAdapter(Context context, MessagesDAO messagesDAO) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.messagesDAO = messagesDAO;
    }


    @Override
    public int getItemViewType(int position) {
        MessageEntity messageEntity = messageEntities.get(position);

        return messageEntity.getUserId().equals(XMPPManager.meID) ? 0 : 1;
    }

    public void setListenner(ChatAdapterListener listenner) {
        this.listener = listenner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        if (i == 0) {
            view = inflater.inflate(R.layout.row_chat, viewGroup, false);
            return new ViewHolderMy(view);
        } else {
            view = inflater.inflate(R.layout.row_chat_other, viewGroup, false);
            return new ViewHolderOther(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        MessageEntity messageEntity = messageEntities.get(i);

        if (!messageEntity.getRead()) {
            messageEntity.setRead(true);
            messagesDAO.save(messageEntity);
        }

        ViewHolder holder = (ViewHolder) viewHolder;
        holder.textView.setText(messageEntity.getBody());

        User userEntity = messageEntity.getUserEntity();

        Picasso.with(inflater.getContext()).load(userEntity.getImage()).fit().centerCrop().into(holder.imageView);

        if (holder instanceof ViewHolderOther) {
            ((ViewHolderOther) holder).textViewName.setText(userEntity.getName());
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return messageEntities == null ? 0 : messageEntities.size();
    }

    @Override
    public void onSheetShown() {

    }

    @Override
    public void onSheetItemSelected(MenuItem menuItem) {

        MessageEntity messageEntity = messageEntities.get(indexSelected);

        switch (menuItem.getItemId()) {
            case R.id.menu_mention_user:
                if(listener != null) listener.mentioned(messageEntity.getUserEntity());
                break;
            case R.id.menu_open_private_chat:
                if(listener != null) listener.chatWithUser(messageEntity.getUserEntity() );
                break;
            case R.id.menu_copy:

                break;
            case R.id.menu_open_profile:
                if(listener != null) listener.openProfileUser(messageEntity.getUserEntity() );
                break;
            case R.id.menu_block_user:
                new MaterialDialog.Builder(context)
                        .title("Confirmar")
                        .content("Você tem certeza que deseja bloquer este usuário ?")
                        .positiveText(R.string.agree)
                        .negativeText(R.string.disagree)
                        .show();
                break;
            case R.id.menu_report_user:

                new MaterialDialog.Builder(context)
                        .title("Confirmar")
                        .content("Você tem certeza que deseja reportar este usuário ?")
                        .positiveText(R.string.agree)
                        .negativeText(R.string.disagree)
                        .show();
                break;
        }
    }

    @Override
    public void onSheetDismissed(int i) {
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.row_chat_tv_message)
        TextView textView;
        @Bind(R.id.row_chat_other_iv_user)
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public class ViewHolderMy extends ViewHolder {
        public ViewHolderMy(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolderOther extends ViewHolder {
        @Bind(R.id.row_chat_tv_name)
        TextView textViewName;

        public ViewHolderOther(View itemView) {
            super(itemView);
        }


        @OnClick(R.id.imageButton7)
        public void morePressed() {
            indexSelected = this.getLayoutPosition();
            new BottomSheet.Builder(itemView.getContext())
                    .setSheet(R.menu.bottom_sheet_user_profile)
                    .setTitle("")
                    .setListener(ChatAdapter.this)
                    .show();
        }

    }

    public interface ChatAdapterListener {

        void chatWithUser(User user);

        void mentioned(User user);

        void openProfileUser(User user);
    }

}


