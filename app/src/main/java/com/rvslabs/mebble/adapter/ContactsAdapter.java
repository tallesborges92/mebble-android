package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.db.ContactUser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 08/10/15.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> {

    private Context mContext;
    private List<ContactUser> contacts;

    private Set<String> selectedContacts;
    private SearchContactActivityDelegate delegate;


    public ContactsAdapter(Context mContext,SearchContactActivityDelegate delegate) {
        this.mContext = mContext;
        this.delegate = delegate;
        contacts = new ArrayList<>();
        selectedContacts = new HashSet<>();
    }

    public void setContacts(List<ContactUser> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.row_search_contact, parent, false);

        // Return a new holder instance
        return new ContactViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        ContactUser contactUser = contacts.get(position);

        holder.textViewName.setText(contactUser.getContactName());

        holder.checkBox.setChecked(selectedContacts.contains(contactUser.getUserId()));
    }

    @Override
    public int getItemCount() {
        if(contacts != null) return contacts.size();
        return 0;
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.checkBox2)
        CheckBox checkBox;
        @Bind(R.id.textView77)
        TextView textViewName;
        @Bind(R.id.imageView14)
        ImageView imageView;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int layoutPosition = getLayoutPosition();

            ContactUser contactUser = contacts.get(layoutPosition);

            String userId = contactUser.getUserId();

            if (!selectedContacts.contains(userId)) {
                selectedContacts.add(userId);
            } else {
                selectedContacts.remove(userId);
            }

            delegate.didSelectUsers(new ArrayList<String>(selectedContacts));

            notifyItemChanged(layoutPosition);
        }
    }

    public interface SearchContactActivityDelegate {

        void didSelectUsers(List<String> users);

    }
}
