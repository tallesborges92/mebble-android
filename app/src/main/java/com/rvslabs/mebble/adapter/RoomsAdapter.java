package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.GroupsUtil;
import com.rvslabs.mebble.model.Room;
import com.rvslabs.mebble.xmpp.RoomsIQCategory;
import com.squareup.picasso.Picasso;

import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 30/07/15.
 */
public class RoomsAdapter extends BaseExpandableListAdapter {


    private final Context context;
    private LinkedHashMap<RoomsIQCategory.Category, List<Room>> groups = new LinkedHashMap<>();
    private LayoutInflater inflator;

    public RoomsAdapter(Context context, LinkedHashMap<RoomsIQCategory.Category, List<Room>> groups) {
        this.groups = groups;
        this.context = context;
        inflator = LayoutInflater.from(context);
    }

    public void setGroups(LinkedHashMap<RoomsIQCategory.Category, List<Room>> groups) {
        this.groups = groups;
        notifyDataSetInvalidated();
    }

    public void addRoomsCategory(RoomsIQCategory.Category category, List<Room> rooms) {
        this.groups.put(category, rooms);
        notifyDataSetChanged();
    }

    public RoomsAdapter(Context context) {
        this.context = context;
        groups = GroupsUtil.initGroups();
    }

    @Override
    public int getGroupCount() {
        return groups.keySet().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        RoomsIQCategory.Category category = (RoomsIQCategory.Category) groups.keySet().toArray()[groupPosition];
        return groups.get(category).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater theInflator = LayoutInflater.from(context);

        View view = theInflator.inflate(R.layout.header_rooms, parent, false);

        TextView textView = (TextView) view.findViewById(R.id.textView);

        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(groupPosition);

        RoomsIQCategory.Category category = (RoomsIQCategory.Category) groups.keySet().toArray()[groupPosition];

        textView.setText(category.getName());

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        RoomsIQCategory.Category category = (RoomsIQCategory.Category) groups.keySet().toArray()[groupPosition];
        List rooms = groups.get(category);
        Room room = (Room) rooms.get(childPosition);

        ViewHolder holder;

        if (groupPosition == 0) {
            convertView = inflator.inflate(R.layout.row_rooms_fixed, parent, false);
            holder = new ViewHolderFixed(convertView);

            holder.textViewDistance.setText(room.getUsers() + " pessoas");


        } else {
            convertView = inflator.inflate(R.layout.row_rooms, parent, false);
            holder = new ViewHolderLocal(convertView);

            ((ViewHolderLocal) holder).textViewAddress.setText(room.getAddress());

            holder.textViewDistance.setText(room.getUsers() + " pessoas / " + room.getDistance() + "m de você");

            String imageUrl = room.getImageUrl();
            if (imageUrl != null && !imageUrl.equals("")) {
                Picasso.with(context).load(imageUrl).fit().placeholder(R.drawable.icon_room).fit().centerCrop().into(((ViewHolderLocal) holder).imageView);
            } else {
                Picasso.with(context).load(R.drawable.icon_room).fit().centerCrop().into(((ViewHolderLocal) holder).imageView);
            }


        }

        holder.textViewName.setText(room.getName());

        convertView.setTag(holder);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder {
        @Bind(R.id.distanceTextView)
        TextView textViewDistance;
        @Bind(R.id.roomNameTextView)
        TextView textViewName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ViewHolderFixed extends ViewHolder {
        public ViewHolderFixed(View view) {
            super(view);
        }
    }

    static class ViewHolderLocal extends ViewHolder {
        @Bind(R.id.addressTextView)
        TextView textViewAddress;
        @Bind(R.id.imageView)
        ImageView imageView;

        public ViewHolderLocal(View view) {
            super(view);
        }
    }

}
