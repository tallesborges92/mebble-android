package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.MultipleUsersModel;
import com.rvslabs.mebble.model.db.RoomUserEntity;
import com.rvslabs.mebble.model.db.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rvslabs on 14/08/15.
 */
public class RoomUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final String TAG = RoomUsersAdapter.class.toString();

    private LayoutInflater inflater;
    private RoomUsersAdapterListener roomUsersAdapterListener;
    private List<RoomUserEntity> roomUserEntities;
    private MultipleUsersModel multipleUsersModel;



    public RoomUsersAdapter(Context context, MultipleUsersModel multipleUsersModel,RoomUsersAdapterListener listener) {
        inflater = LayoutInflater.from(context);
        this.multipleUsersModel = multipleUsersModel;
        this.roomUsersAdapterListener = listener;
    }

    public void setRoomUserEntities(List<RoomUserEntity> roomUserEntities) {
        int oldSize = this.roomUserEntities == null ? 0 : this.roomUserEntities.size();
        int newSize = roomUserEntities.size();
        this.roomUserEntities = roomUserEntities;

        if (newSize > oldSize) {
            notifyItemInserted(roomUserEntities.size() - 1);
        } else {
            notifyItemRemoved(roomUserEntities.size() - 1);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_HEADER) {
            View view = inflater.inflate(R.layout.header_about_rooms, viewGroup, false);
            return new ViewHeader(view);
        }

        View view = inflater.inflate(R.layout.row_room_users, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int i) {

        if (vh instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) vh;

            RoomUserEntity roomUserEntity = roomUserEntities.get(i - 1);

            User user = roomUserEntity.getUserEntity();

            viewHolder.textView.setText(user.getName());
            if (user.getImage() != null && !user.getImage().equals("")) {
                Picasso.with(inflater.getContext()).load(user.getImage()).fit().centerCrop().into(viewHolder.imageView);
            }
        } else if (vh instanceof ViewHeader) {
            ViewHeader header = (ViewHeader) vh;

            if (multipleUsersModel.getImage() != null && !multipleUsersModel.getImage().equals("")) {
                Picasso.with(inflater.getContext()).load(multipleUsersModel.getImage()).fit().centerCrop().into(header.imageView);
            } else {
                Picasso.with(inflater.getContext()).load(R.drawable.img_about_room).fit().centerCrop().into(header.imageView);
            }
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return roomUserEntities == null ? 1 : roomUserEntities.size() + 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.row_room_users_tv_user_name)
        TextView textView;
        @Bind(R.id.row_room_users_iv_user)
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ViewHeader extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView4)
        ImageView imageView;

        @Bind(R.id.header_about_room_ln_invite)
        LinearLayout inviteLinearLayout;
        @Bind(R.id.header_about_room_ln_mute)
        LinearLayout muteLinearLayout;
        @Bind(R.id.header_about_room_ln_denounce)
        LinearLayout denounceLinearLayout;
        @Bind(R.id.header_about_room_ln_share)
        LinearLayout shareLinearLayout;

        public ViewHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.header_about_room_ln_invite)
        public void invitePressed() {
            Log.d(TAG, "invitePressed() called with: " + "");
            roomUsersAdapterListener.inviteUsersClicked();
        }

        @OnClick(R.id.header_about_room_btn_leave_room)
        public void leaveRoom() {
            Log.d(TAG, "leaveRoom() called with: " + "");
            roomUsersAdapterListener.leaveRoomClicked();
        }

        @OnClick(R.id.header_about_room_ln_denounce)
        public void denounce() {
            Log.d(TAG, "denounce() called with: " + "");
            roomUsersAdapterListener.denounceClicked();
        }

        @OnClick(R.id.header_about_room_ln_share)
        public void share() {
            Log.d(TAG, "share() called with: " + "");
            roomUsersAdapterListener.shareClicked();
        }

        @OnClick(R.id.header_about_room_ln_mute)
        public void mute() {
            Log.d(TAG, "mute() called with: " + "");
            roomUsersAdapterListener.silenceClicked();
        }
    }

    public interface RoomUsersAdapterListener {

        void inviteUsersClicked();

        void leaveRoomClicked();

        void denounceClicked();

        void silenceClicked();

        void shareClicked();

    }

}
