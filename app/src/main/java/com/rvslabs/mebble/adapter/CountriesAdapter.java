package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.api.mebble.CountriesResponse;
import com.rvslabs.mebble.model.api.mebble.Country;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 01/09/15.
 */
public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {

    private LayoutInflater inflater;

    CountriesResponse countriesResponse;
    public OnItemClickListener mItemClickListener;

    public CountriesAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setCountriesResponse(CountriesResponse countriesResponse) {
        this.countriesResponse = countriesResponse;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_country, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Country country = countriesResponse.getCountries().get(position);
        holder.textView.setText(country.getName());
    }

    @Override
    public int getItemCount() {
        return (countriesResponse == null) ? 0 : countriesResponse.getCountries().size();
    }

    public Country getCountry(int position) {
        return countriesResponse.getCountries().get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.textView17)
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getLayoutPosition());
            }
        }
    }
}
