package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 15/10/15.
 */
public class ContactsCursorAdapter extends CursorAdapter {

    LayoutInflater inflater;
    Map<String, String> headerSections = new HashMap<>();

    public ContactsCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.row_contact, parent, false);
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        headerSections = new HashMap<>();
        return super.swapCursor(newCursor);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();

        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
        String userId = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
        String firstLetter;

        if (name.matches("[a-zA-Z].*")) {
            firstLetter = String.valueOf(name.charAt(0)).toUpperCase();
        } else {
            firstLetter = "#";
        }


        String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));

        if (photoUri != null) {
            holder.firstLetterTextView.setVisibility(View.INVISIBLE);
            holder.userImageView.setVisibility(View.VISIBLE);
            Picasso.with(context).load(photoUri).into(holder.userImageView);
        } else {
            holder.userImageView.setVisibility(View.INVISIBLE);
            holder.firstLetterTextView.setVisibility(View.VISIBLE);
            holder.firstLetterTextView.setText(firstLetter);
        }

        if (headerSections.get(firstLetter) == null) {
            headerSections.put(firstLetter, userId);
        }

        if (headerSections.get(firstLetter).equals(userId)) {
            holder.headerDivisorView.setVisibility(View.VISIBLE);
            holder.headerFirstLetterTextView.setVisibility(View.VISIBLE);
            holder.headerFirstLetterTextView.setText(firstLetter);
        } else {
            holder.headerDivisorView.setVisibility(View.GONE);
            holder.headerFirstLetterTextView.setVisibility(View.GONE);
        }

        holder.nameTextView.setText(name);
    }

    static class ViewHolder {
        @Bind(R.id.row_contact_tv_name)
        TextView nameTextView;
        @Bind(R.id.row_contact_tv_first_letter)
        TextView firstLetterTextView;
        @Bind(R.id.row_contact_iv_user)
        ImageView userImageView;
        @Bind(R.id.row_contact_tv_header_first_letter)
        TextView headerFirstLetterTextView;
        @Bind(R.id.row_contact_v_header_divisor)
        View headerDivisorView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
