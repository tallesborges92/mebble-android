package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.Group;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.dao.GroupDAO;
import com.rvslabs.mebble.model.db.dao.UserDAO;
import com.squareup.picasso.Picasso;

import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.util.JidUtil;
import org.jxmpp.stringprep.XmppStringprepException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rvslabs on 10/09/15.
 */
public class ConversationsAdapter extends ArrayAdapter<MessageEntity> {

    private static final String TAG = ConversationsAdapter.class.toString();
    private final LayoutInflater inflater;
    @Inject UserDAO userDao;
    @Inject GroupDAO groupDao;

    public ConversationsAdapter(Context context, int resource) {
        super(context, resource);
        inflater = LayoutInflater.from(getContext());
        SampleApp.getAppComponent().inject(this);
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.row_conversations, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        MessageEntity m = getItem(position);
        String image = null;
        String name = null;
        String message = null;

        if (m.getComponent().equals("localhost")) {
            User userEntity = null;
            if (m.getUserId().equals(XMPPManager.meID)) {

                try {
                    BareJid bareJid;
                    if (JidUtil.isValidBareJid(m.getRecipientBareJID())) {
                        bareJid = JidCreate.bareFrom(m.getRecipientBareJID());
                    } else {
                        bareJid = JidCreate.bareFrom(m.getRecipientBareJID() + "@localhost");
                    }

                    userEntity = userDao.getUser(bareJid.getLocalpart());
                } catch (XmppStringprepException e) {
                    e.printStackTrace();
                }

            } else {
                userEntity = m.getUserEntity();
            }
            assert userEntity != null;

            image = userEntity.getImage();
            name = userEntity.getName();
        }else if (m.getComponent().equals("groupchat.localhost")) {
            try {
                Group group = groupDao.getGroup(m.getBareJid());

                name = group.getName();
                image = group.getImage();

                Log.i(TAG, m.toString());
            } catch (Exception ignored) {

            }
        }

        message = m.getBody();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        String formatedDate = dateFormat.format(new Date(m.getDate()));

        holder.userName.setText(name);
        holder.userMessage.setText(message);
        holder.houtLastMessageTv.setText(formatedDate);
        if (m.getTotalNotReadMessages() == 0) {
            holder.countNotReadMessagesTv.setVisibility(View.GONE);
        } else {
            holder.countNotReadMessagesTv.setVisibility(View.VISIBLE);
            holder.countNotReadMessagesTv.setText("" + m.getTotalNotReadMessages());
        }
        Picasso.with(inflater.getContext()).load(image).fit().centerCrop().into(holder.imageView);

        return view;
    }

    static class ViewHolder {
        @Bind(R.id.conversation_tv_name)
        TextView userName;

        @Bind(R.id.conversation_tv_last_message)
        TextView userMessage;

        @Bind(R.id.conversation_iv)
        RoundedImageView imageView;

        @Bind(R.id.conversation_tv_hour_last_message)
        TextView houtLastMessageTv;

        @Bind(R.id.conversation_tv_count_not_read_messages)
        TextView countNotReadMessagesTv;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
