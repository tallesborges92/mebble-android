package com.rvslabs.mebble.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableStringBuilder;

import com.rvslabs.mebble.fragments.ContactsFragment;
import com.rvslabs.mebble.fragments.ConversationsFragment;
import com.rvslabs.mebble.fragments.RoomsFragment;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by rvslabs on 19/08/15.
 */
public class MainPageAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Salas", "Conversas", "Contatos" };
    private Context context;

    private HashMap<Integer, Fragment> mPageReference = new HashMap<>();


    public MainPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RoomsFragment roomsFragment = RoomsFragment.newInstance();
                mPageReference.put(position, roomsFragment);
                return roomsFragment;
            case 1:
                ConversationsFragment conversationsFragment = ConversationsFragment.newInstance();
                mPageReference.put(position, conversationsFragment);
                return conversationsFragment;
            case 2:
                ContactsFragment contactsFragment = ContactsFragment.newInstance();
                mPageReference.put(position, contactsFragment);
                return contactsFragment;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        CharSequence text = tabTitles[position];
        CalligraphyTypefaceSpan titleType =  new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), "fonts/OpenSans-Semibold.ttf"));;
        SpannableStringBuilder s = new SpannableStringBuilder(text);
        s.setSpan(titleType, 0, text.length(), 0);
        return s;
    }

    public Fragment getFragment(int p) {
        return mPageReference.get(p);
    }
}
