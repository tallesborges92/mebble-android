package com.rvslabs.mebble.activities.rooms;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.google.android.gms.maps.model.LatLng;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.exceptions.MebbleErrorException;
import com.rvslabs.mebble.model.Room;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.util.Constants;
import com.rvslabs.mebble.util.Util;
import com.rvslabs.mebble.xmpp.MebbleErrorRoomExist;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.parceler.Parcels;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateRoomActivity extends AppCompatActivity {

    private static final String TAG = "CreateRoom";
    public static final int REQUEST_PLACE_CODE = 666;
    public static final int PHOTO_SELECTED = 123;

    @Bind(R.id.textView9)
    TextView placeTv;

    @Bind(R.id.editText2)
    EditText editTextName;

    @Bind(R.id.editText4)
    EditText editTextDescription;

    @Bind(R.id.activity_create_room_floatBtn)
    FloatingActionButton addPhotoBtn;

    @Bind(R.id.imageView3)
    ImageView imageView;
    private TransferUtility transferUtility;
    private String imageUrl;
    private LatLng latLng;
    private String placeId;
    private String address;
    private int range;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle("Criar Sala");

        transferUtility = Util.getTransferUtility();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_room, menu);
        return true;
    }

    @OnClick(R.id.activity_create_room_floatBtn)
    public void addPhotoPressed() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PHOTO_SELECTED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.done:

                createRoom();

                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createRoom() {
        if (latLng != null && !name.isEmpty()) {
            final Room room = new Room();
            room.setName(editTextName.getText().toString());
            room.setDescription(editTextDescription.getText().toString());
            room.setLatitude(Float.valueOf(String.valueOf(latLng.latitude)));
            room.setLongitude(Float.valueOf(String.valueOf(latLng.longitude)));
            room.setAddress(name);
            room.setRange(range);
            room.setPlaceId(placeId);

            if (imageUrl != null) {
                room.setImageUrl(imageUrl);
            }

            try {
                Room roomCreated = XMPPManager.createRoom(room);
                Toast.makeText(CreateRoomActivity.this, "Sala Criada com sucesso!", Toast.LENGTH_SHORT).show();
                finish();
            } catch (SmackException.NotConnectedException | XMPPException.XMPPErrorException | SmackException.NoResponseException e) {
                Log.e(TAG, "onOptionsItemSelected " + e.toString());
                Toast.makeText(CreateRoomActivity.this, "Erro ao criar Sala.", Toast.LENGTH_SHORT).show();
            } catch (MebbleErrorException e) {

                final MebbleErrorRoomExist error = (MebbleErrorRoomExist) e.getMebbleErrorResult().getErrors().get(0);


                new MaterialDialog.Builder(this)
                        .title(R.string.room_exists)
                        .content(R.string.room_exists_content)
                        .items(R.array.rooms_exist).itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                        if (i == 1) {
                            try {
                                Room room = XMPPManager.getRoomById(error.getOtherRoomId());
                                Intent intent = new Intent(CreateRoomActivity.this, RoomChatActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("multipleUsersModel", Parcels.wrap(room));
                                startActivity(intent);
                            } catch (MebbleErrorException e1) {
                                Toast.makeText(CreateRoomActivity.this, "Error finding room", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }).show();

                Log.e(TAG, "onOptionsItemSelected " + e.toString());
                Toast.makeText(CreateRoomActivity.this, "Erro ao criar Sala :" + e.getMebbleErrorResult().getErrors().get(0).getErrorDescription(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @OnClick(R.id.textView9)
    public void submit() {
        Intent intent = new Intent(this, ChooseLocalActivity.class);
        startActivityForResult(intent, REQUEST_PLACE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_PLACE_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                latLng = data.getParcelableExtra("latLng");

                name = data.getStringExtra("name");
                placeId = data.getStringExtra("placeId");
                address = data.getStringExtra("address");
                range = data.getIntExtra("range", 0);

                placeTv.setText(name + " (" + range + "m)");
            }
        } else if (requestCode == PHOTO_SELECTED) {
            if (resultCode == RESULT_OK) {

                Bitmap bitmap = Util.getBitmapFromURI(this, data);
                Bitmap resizedImage = Util.resizeImage(bitmap);

                try {
                    File fileResized = Util.createFile(resizedImage);

                    Util.uploadFile(fileResized, Constants.BUCKET_NAME_ROOMS, UUID.randomUUID().toString() + ".jpg", new Util.UploadToAmazonListener() {
                        @Override
                        public void uploadCompleted(String url) {
                            imageUrl = url;
                            Toast.makeText(CreateRoomActivity.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void uploadError() {
                            Toast.makeText(CreateRoomActivity.this, "Upload Error", Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (IOException e) {
                    Toast.makeText(CreateRoomActivity.this, "Error uploading image", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onActivityResult " + e.toString());
                }

                imageView.setImageBitmap(resizedImage);
            }
        }
    }

}
