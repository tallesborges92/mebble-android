package com.rvslabs.mebble.activities.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.rvslabs.mebble.R;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public class StartActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://192.168.100.234:9090";

    private static final int REQUEST_COUNTRY_DDI = 666;
    private static final String TAG = "StartActivity";

    @Bind(R.id.editText5)
    EditText ddiEditText;

    @Bind(R.id.editText6)
    EditText numberEditText;
    private String region;
    private String ddi;
    private MebbleApiInterface apiService;

    @OnClick(R.id.textView15)
    public void selectCountryPressed() {
        Intent i = new Intent(this, CountriesActivity.class);
        startActivityForResult(i, REQUEST_COUNTRY_DDI);
    }

    @OnClick(R.id.button2)
    public void confirmPressed() {
        String ddi = ddiEditText.getText().toString();
        String phone = numberEditText.getText().toString();

        String numberPhone = ddi + phone;

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(numberPhone, region);

            Toast.makeText(StartActivity.this, "Valid Number" + phoneNumber.toString(), Toast.LENGTH_SHORT).show();

            SharedPreferences pref =
                    PreferenceManager.getDefaultSharedPreferences(this);
            final SharedPreferences.Editor edit = pref.edit();

            edit.putString("ddi", String.valueOf(phoneNumber.getCountryCode()));
            edit.putString("number", String.valueOf(phoneNumber.getNationalNumber()));
            edit.apply();

            Long authenticateMillis = pref.getLong("authenticateMillis", -1);

            boolean sendNewAuthenticate;

            if (authenticateMillis == -1) {
                sendNewAuthenticate = true;
            } else {
                Long difTime = System.nanoTime() - authenticateMillis;
                Long minutes = TimeUnit.MINUTES.convert(difTime, TimeUnit.NANOSECONDS);

                sendNewAuthenticate = (minutes >= 5);
            }

            if (!sendNewAuthenticate) {
                startActivityVerify();
            } else {
                apiService.authenticate(String.valueOf(phoneNumber.getCountryCode()), String.valueOf(phoneNumber.getNationalNumber()), new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        Toast.makeText(StartActivity.this, "Success Authenticate", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "success " + response);
                        edit.putLong("authenticateMillis", System.nanoTime());
                        edit.commit();

                        startActivityVerify();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(StartActivity.this, "Error Authenticate", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "failure " + error);
                    }
                });
            }

        } catch (NumberParseException e) {
            Log.e(TAG, "confirmPressed " + e);
            Toast.makeText(StartActivity.this, "error", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(StartActivity.this, numberPhone, Toast.LENGTH_SHORT).show();
    }

    public void startActivityVerify() {
        Intent i = new Intent(this, VerifyAuthenticateActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        apiService = restAdapter.create(MebbleApiInterface.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_COUNTRY_DDI) {
                ddi = data.getStringExtra("ddi");
                region = data.getStringExtra("reigon");
                ddiEditText.setText("+" + ddi);
            }
        }
    }

    interface MebbleApiInterface {
        @FormUrlEncoded
        @POST("/plugins/mebblePlugin/v1/authentication/authenticate")
        void authenticate(@Field("ddi") String ddi, @Field("number") String number, Callback<Object> cb);
    }
}

