package com.rvslabs.mebble.activities.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.activities.settings.MyProfileActivity;
import com.rvslabs.mebble.model.api.mebble.AuthenticationVerifyResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public class VerifyAuthenticateActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://192.168.100.234:9090";
    private static final String TAG = "VerifyAuthenticate";
    MebbleApiInterface apiService;

    @Bind(R.id.editText7)
    EditText editText;


    @OnClick(R.id.button3)
    public void confirmCodePressed() {

        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor edit = pref.edit();

        String ddi = pref.getString("ddi", "");
        String number = pref.getString("number", "");
        String code = editText.getText().toString();

        apiService.authenticate(ddi, number, code, new Callback<AuthenticationVerifyResponse>() {
            @Override
            public void success(AuthenticationVerifyResponse authenticationResponse, Response response) {
                Toast.makeText(VerifyAuthenticateActivity.this, "Authentication Success", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "success " + response);

                edit.putString("id", authenticationResponse.getId());
                edit.putString("password", authenticationResponse.getPassword());
                edit.putBoolean("verified", true);
                edit.apply();

                Intent i = new Intent(VerifyAuthenticateActivity.this, MyProfileActivity.class);
                i.putExtra("initialProfile", true);
                startActivity(i);

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(VerifyAuthenticateActivity.this, "Error Authenticate", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "failure " + error);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_authenticate);
        ButterKnife.bind(this);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        apiService = restAdapter.create(MebbleApiInterface.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verify_authenticate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    interface MebbleApiInterface {
        @FormUrlEncoded
        @POST("/plugins/mebblePlugin/v1/authentication/verify")
        void authenticate(@Field("ddi") String ddi, @Field("number") String number, @Field("code") String code, Callback<AuthenticationVerifyResponse> cb);
    }
}
