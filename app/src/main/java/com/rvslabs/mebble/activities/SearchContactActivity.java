package com.rvslabs.mebble.activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.adapter.ContactsAdapter;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.ContactUser;
import com.rvslabs.mebble.model.db.dao.UserDAO;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchContactActivity extends AppCompatActivity {

    private static final String TAG = SearchContactActivity.class.toString();

    @Bind(R.id.activity_search_contact_rv_contacts)
    RecyclerView contacts;

    @Inject
    UserDAO userDAO;
    private ContactsAdapter contactsAdapter;

    private List<String> selectedUsers;

    private MultiUserChat multiUserChat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);

        ButterKnife.bind(this);
        SampleApp.getAppComponent().inject(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        String multiUserModelJid = getIntent().getStringExtra("jid");

        contactsAdapter = new ContactsAdapter(this, new MySearchContactActivityDelegate());

        contacts.setAdapter(contactsAdapter);
        contacts.setLayoutManager(new LinearLayoutManager(this));

        contactsAdapter.setContacts(userDAO.getContactUsers());

        MultiUserChatManager chatManager = MultiUserChatManager.getInstanceFor(XMPPManager.connection);

        multiUserChat = chatManager.getMultiUserChat(multiUserModelJid);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.done:
                try {
                    for (String userId : selectedUsers) {
                        multiUserChat.invite(userId + "@localhost", "");
                        Log.d(TAG, "onOptionsItemSelected() called with: " + "item = [" + userId + "]");
                    }
                    Toast.makeText(SearchContactActivity.this, "Usuarios Convidados com sucesso", Toast.LENGTH_SHORT).show();
                } catch (SmackException.NotConnectedException ignored) {
                    Toast.makeText(SearchContactActivity.this, "Erro ao convidar usuarios", Toast.LENGTH_SHORT).show();
                }

                finish();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_contacts, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // perform query here
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<ContactUser> contactUsers;

                if (newText.equals("")) {
                    contactUsers = userDAO.getContactUsers();
                } else {
                    contactUsers = userDAO.getContactUsersByName(newText);
                }

                contactsAdapter.setContacts(contactUsers);
                return false;
            }
        });

        return true;
    }


    private class MySearchContactActivityDelegate implements ContactsAdapter.SearchContactActivityDelegate {
        @Override
        public void didSelectUsers(List<String> users) {
            selectedUsers = users;
        }
    }
}
