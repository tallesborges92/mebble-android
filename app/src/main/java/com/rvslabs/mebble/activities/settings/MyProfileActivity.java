package com.rvslabs.mebble.activities.settings;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.util.Constants;
import com.rvslabs.mebble.util.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.SmackException;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyProfileActivity extends AppCompatActivity {

    public static final int PHOTO_SELECTED = 123;
    private static final String TAG = "MyProfileActivity";


    @Bind(R.id.activity_my_perfil_ll_telephone)
    LinearLayout telephoneLL;
    @Bind(R.id.activity_my_perfil_ll_btn_privacy)
    LinearLayout privacyLL;
    @Bind(R.id.activity_my_perfil_btn_delete_perfil)
    Button deleteProfileBtn;
    @Bind(R.id.activity_my_perfil_floatBtn)
    FloatingActionButton fltButton;
    @Bind(R.id.activity_my_perfil_img_perfil)
    ImageView perfilImage;
    @Bind(R.id.activity_my_perfil_txt_name)
    TextView txtName;
    @Bind(R.id.activity_my_perfil_txt_number)
    TextView numberTxt;

    private String urlImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);

        if (XMPPManager.connection == null || (XMPPManager.connection != null && !XMPPManager.connection.isConnected())) {
            XMPPManager.getInstance().connectToXMPP();
        }

        boolean initialProfile = getIntent().getBooleanExtra("initialProfile", false);

        if (initialProfile) {
            telephoneLL.setVisibility(View.GONE);
            privacyLL.setVisibility(View.GONE);
            deleteProfileBtn.setVisibility(View.GONE);
        } else {

            User user = XMPPManager.getUserInfo(XMPPManager.meID);
            if (user != null) {
                txtName.setText(user.getName());
                Picasso.with(this).load(user.getImage()).fit().centerCrop().into(perfilImage);
                numberTxt.setText(XMPPManager.getUserNumber());

                Log.i(TAG, "onCreate " + user);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.done) {
            if (urlImage != null) {
                try {
                    XMPPManager.setUserNameAndImage(txtName.getText().toString(), urlImage);
                    Toast.makeText(MyProfileActivity.this, "User Updated", Toast.LENGTH_SHORT).show();
                } catch (SmackException.NotConnectedException e) {
                    Toast.makeText(MyProfileActivity.this, "Error Update", Toast.LENGTH_SHORT).show();
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.activity_my_perfil_ll_btn_privacy)
    public void privacyPressed() {
        Intent i = new Intent(this, PrivacySettingsActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.activity_my_perfil_floatBtn)
    public void floatBtnPressed() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PHOTO_SELECTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PHOTO_SELECTED) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();

                // TODO : UPLOAD WHEN SELECT PRESS DONE
                Picasso.with(this).load(selectedImage).fit().centerCrop().into(perfilImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        try {
                            File file = Util.createFile(((BitmapDrawable) perfilImage.getDrawable()).getBitmap());

                            Util.uploadFile(file, Constants.BUCKET_NAME_USERS, XMPPManager.meID + ".jpg", new Util.UploadToAmazonListener() {
                                @Override
                                public void uploadCompleted(String url) {
                                    Log.d(TAG, "uploadCompleted " + url);

                                    urlImage = url;
                                }

                                @Override
                                public void uploadError() {
                                    Log.e(TAG, "uploadError ");
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });

            }
        }
    }


}


