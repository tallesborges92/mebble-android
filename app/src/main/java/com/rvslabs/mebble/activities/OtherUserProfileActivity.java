package com.rvslabs.mebble.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.dao.UserDAO;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OtherUserProfileActivity extends AppCompatActivity {

    @Bind(R.id.activity_other_user_profile_user_image)
    ImageView userImageView;

    @Bind(R.id.activity_other_user_profile_user_name)
    TextView nameTextView;

    @Inject UserDAO userDAO;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user_profile);
        ButterKnife.bind(this);
        SampleApp.getAppComponent().inject(this);

        String userId = getIntent().getStringExtra("userId");

        user = userDAO.getUser(userId);

        Picasso.with(this).load(user.getImage()).fit().centerCrop().into(userImageView);
        nameTextView.setText(user.getName());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_other_user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
