package com.rvslabs.mebble.activities.rooms;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.activities.ChatActivity;
import com.rvslabs.mebble.activities.OtherUserProfileActivity;
import com.rvslabs.mebble.adapter.ChatAdapter;
import com.rvslabs.mebble.model.MultipleUsersModel;
import com.rvslabs.mebble.model.Room;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.MessageWithUserGetResolver;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.functions.Action1;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class RoomChatActivity extends AppCompatActivity {

    private static final String TAG = "RoomChatActivity";
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.editText)
    EditText editText;

    @Bind(R.id.activity_room_chat_ll_out)
    LinearLayout linearLayoutOut;

    private ChatAdapter adapter;
    private MultipleUsersModel multipleUsersModel;
    private MultiUserChat multiUserChat;

    @Inject
    StorIOSQLite storIOSQLite;

    @Inject
    MessagesDAO messagesDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_chat);
        ButterKnife.bind(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        SampleApp.getAppComponent().inject(this);

        multipleUsersModel = Parcels.unwrap(getIntent().getParcelableExtra("multipleUsersModel"));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(multipleUsersModel.getName());

        MultiUserChatManager chatManager = MultiUserChatManager.getInstanceFor(XMPPManager.connection);

        multiUserChat = chatManager.getMultiUserChat(multipleUsersModel.getJID());

        adapter = new ChatAdapter(this,messagesDAO);
        adapter.setListenner(new ChatAdapterListener());

        recyclerView.setAdapter(adapter);

        linearLayoutOut.setVisibility(View.GONE);
        editText.setEnabled(true);

        if (multipleUsersModel instanceof Room) {

            Room room = (Room) multipleUsersModel;

            if (room.getLatitude() != null && room.getLongitude() != null) {
                Location roomLocation = new Location("Room");
                roomLocation.setLatitude(room.getLatitude());
                roomLocation.setLongitude(room.getLongitude());

                Location userLocation = XMPPManager.getInstance().getLocationManager().getLastLocation();

                if (userLocation != null) {

                    float distance = userLocation.distanceTo(roomLocation);

                    Log.d(TAG, "Distance " + distance);

                    if (distance > room.getRange()) {
                        linearLayoutOut.setVisibility(View.VISIBLE);
                        editText.setEnabled(false);
                    } else {
                        linearLayoutOut.setVisibility(View.GONE);
                        editText.setEnabled(true);
                    }
                }
            }
        }

        final Subscription subscription = storIOSQLite
                .get()
                .listOfObjects(MessageEntity.class)
                .withQuery(RawQuery.builder()
                        .query("SELECT * FROM messages JOIN users ON (messages.userId = users.id) where messages.barejid = ?")
                        .observesTables("messages")
                        .args(multipleUsersModel.getJID())
                        .build())
                .withGetResolver(new MessageWithUserGetResolver())
                .prepare()
                .createObservable()
                .observeOn(mainThread())
                .subscribe(new Action1<List<MessageEntity>>() {
                    @Override
                    public void call(List<MessageEntity> messageEntities) {
                        adapter.setMessageEntities(messageEntities);
                        recyclerView.smoothScrollToPosition(messageEntities.size());

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        // In cases when you are not sure that query will be successful
                        // You can prevent crash of the application via error handler

                    }
                });


        editText.setOnEditorActionListener(new SendMessageActionListener());

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (!multiUserChat.isJoined()) {
                        multiUserChat.createOrJoin(XMPPManager.meID);
                    } else {
                        multiUserChat.join(XMPPManager.meID);
                    }
                } catch (XMPPException.XMPPErrorException | SmackException e) {
                    Log.e(null, null, e);
                } finally {
                    if (multipleUsersModel instanceof Room) {
                        XMPPManager.sendInRoom((Room) multipleUsersModel);
                    }
                }
                return null;
            }

        }.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_about_room:
                Intent intent = new Intent(this, AboutRoomActivity.class);
                intent.putExtra("multipleUsersModel", Parcels.wrap(multipleUsersModel));
                startActivity(intent);
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    private class ChatAdapterListener implements ChatAdapter.ChatAdapterListener {
        @Override
        public void chatWithUser(User user) {
            Intent i = new Intent(RoomChatActivity.this, ChatActivity.class);
            i.putExtra("userId", user.getId());
            startActivity(i); // brings up the second activity
        }

        @Override
        public void mentioned(User user) {
            editText.setText(editText.getText() + " @" + user.getName());
        }

        @Override
        public void openProfileUser(User user) {
            Intent i = new Intent(RoomChatActivity.this, OtherUserProfileActivity.class);
            i.putExtra("userId", user.getId());
            startActivity(i); // brings up the second activity
        }
    }

    private class SendMessageActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            String text = editText.getText().toString();
            if (text.equals("")) {
                return false;
            }
            editText.setText("");

            try {
                multiUserChat.sendMessage(text);
            } catch (SmackException.NotConnectedException e) {
                Log.d("AsyncTask", e.toString());
            }

            return true;
        }
    }
}
