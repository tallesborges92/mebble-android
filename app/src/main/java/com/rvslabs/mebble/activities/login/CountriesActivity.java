package com.rvslabs.mebble.activities.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.adapter.CountriesAdapter;
import com.rvslabs.mebble.model.api.mebble.CountriesResponse;
import com.rvslabs.mebble.model.api.mebble.Country;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.GET;

public class CountriesActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://192.168.100.234:9090";
    private static final String TAG = "Countries Activity";


    @Bind(R.id.recyclerView3)
    RecyclerView recyclerView;
    private CountriesAdapter countriesAdapter;
    private MebbleApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        ButterKnife.bind(this);

        countriesAdapter = new CountriesAdapter(this);
        countriesAdapter.mItemClickListener = new CountriesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Country country = countriesAdapter.getCountry(position);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("ddi", country.getCallingCode());
                resultIntent.putExtra("region", country.getAlpha2Code());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        };

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(countriesAdapter);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        apiService = restAdapter.create(MebbleApiInterface.class);

        apiService.getCountries(new Callback<CountriesResponse>() {
            @Override
            public void success(CountriesResponse countriesResponse, Response response) {
                Toast.makeText(CountriesActivity.this, "Success", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "success " + response);
                countriesAdapter.setCountriesResponse(countriesResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(CountriesActivity.this, "Error", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "failure " + error);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_countries, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

interface MebbleApiInterface {
    @GET("/plugins/mebblePlugin/v1/util/countries")
    void getCountries(Callback<CountriesResponse> cb);
}