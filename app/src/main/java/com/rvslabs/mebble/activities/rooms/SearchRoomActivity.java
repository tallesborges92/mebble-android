package com.rvslabs.mebble.activities.rooms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.adapter.RoomAdapter;
import com.rvslabs.mebble.model.Room;
import com.rvslabs.mebble.model.XMPPManager;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchRoomActivity extends AppCompatActivity {

    @Bind(R.id.activity_search_room_rv_rooms)
    RecyclerView recyclerView;
    private RoomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_room);

        ButterKnife.bind(this);

        adapter = new RoomAdapter();
        adapter.setListener(new RoomAdapter.OnRoomClickListener() {
            @Override
            public void onRoomClick(Room room, int position) {
                Intent intent = new Intent(SearchRoomActivity.this, RoomChatActivity.class);
                intent.putExtra("multipleUsersModel", Parcels.wrap(room));
                startActivity(intent);
            }
        });
        // Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_room, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // perform query here
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Room> rooms = XMPPManager.getRoomsByName(newText);
                adapter.setRooms(rooms);
                return false;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
