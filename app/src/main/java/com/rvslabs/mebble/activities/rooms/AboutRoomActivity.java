package com.rvslabs.mebble.activities.rooms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.activities.MainActivity;
import com.rvslabs.mebble.activities.SearchContactActivity;
import com.rvslabs.mebble.adapter.RoomUsersAdapter;
import com.rvslabs.mebble.model.MultipleUsersModel;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.RoomUserEntity;
import com.rvslabs.mebble.model.db.RoomUserGetResolver;

import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class AboutRoomActivity extends AppCompatActivity {

    @Bind(R.id.activity_about_room_lv_users)
    RecyclerView usersRV;

    @Inject
    StorIOSQLite storIOSQLite;

    private RoomUsersAdapter adapter;
    private MultipleUsersModel multipleUsersModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_room);
        ButterKnife.bind(this);

        SampleApp.getAppComponent().inject(this);

        multipleUsersModel = Parcels.unwrap(getIntent().getParcelableExtra("multipleUsersModel"));

        RoomUsersAdapter.RoomUsersAdapterListener listener = new RoomUsersAdapter.RoomUsersAdapterListener() {
            @Override
            public void inviteUsersClicked() {
                Intent view = new Intent(AboutRoomActivity.this, SearchContactActivity.class);
                view.putExtra("jid", multipleUsersModel.getJID());
                startActivity(view);
            }

            @Override
            public void leaveRoomClicked() {
                showLeaveRoomDialog();
            }

            @Override
            public void denounceClicked() {
                showDenounceDialog();
            }

            @Override
            public void silenceClicked() {
                showMuteDialog();
            }

            @Override
            public void shareClicked() {

            }
        };

        adapter = new RoomUsersAdapter(this, multipleUsersModel,listener);

        setTitle(multipleUsersModel.getName());

        usersRV.setLayoutManager(new LinearLayoutManager(this));
        usersRV.setAdapter(adapter);

        try {
            BareJid jid = JidCreate.bareFrom(multipleUsersModel.getJID());

            storIOSQLite
                    .get()
                    .listOfObjects(RoomUserEntity.class)
                    .withQuery(RawQuery.builder()
                            .query("SELECT * FROM room_users JOIN users ON (room_users.userId = users.id) where room_users.roomId = ?")
                            .observesTables("room_users")
                            .args(jid.getLocalpart())
                            .build())
                    .withGetResolver(new RoomUserGetResolver())
                    .prepare()
                    .createObservable()
                    .observeOn(mainThread())
                    .subscribe(new Action1<List<RoomUserEntity>>() {
                        @Override
                        public void call(List<RoomUserEntity> roomUserEntities) {
                            adapter.setRoomUserEntities(roomUserEntities);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            // In cases when you are not sure that query will be successful
                            // You can prevent crash of the application via error handler

                        }
                    });

        } catch (Exception e) {
            Log.e(null, null, e);
        }

    }

    private void showLeaveRoomDialog() {
        new MaterialDialog.Builder(AboutRoomActivity.this)
                .title("Confirmação")
                .content("Você quer mesmo sair deste grupo/sala ?")
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        XMPPManager.getInstance().leaveConference(multipleUsersModel);
                        Intent i = new Intent(AboutRoomActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //    Dialogs
    private void showDenounceDialog() {
        new MaterialDialog.Builder(this)
                .title("Motivo da Denúncia")
                .items(R.array.denounce_room)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/
                        return true;
                    }
                })
                .negativeText("CANCELAR")
                .positiveText("BLOQUEAR")
                .show();

    }

    private void showMuteDialog() {

        new MaterialDialog.Builder(AboutRoomActivity.this)
                .title("Silenciar Sala")
                .content("Ao silenciar a sala você não receberá mais notificações desta.")
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .show();

    }


}
