package com.rvslabs.mebble.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.adapter.ChatAdapter;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.MessageWithUserGetResolver;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;
import com.rvslabs.mebble.model.db.dao.UserDAO;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jxmpp.jid.util.JidUtil;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.functions.Action1;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class ChatActivity extends AppCompatActivity {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.editText)
    EditText editText;
    private ChatAdapter adapter;

    private String userId;

    @Inject
    StorIOSQLite storIOSQLite;

    @Inject
    MessagesDAO messagesDAO;

    @Inject
    UserDAO userDAO;

    private ChatManager chatmanager;
    private Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        SampleApp.getAppComponent().inject(this);

        userId = getIntent().getStringExtra("userId");
        adapter = new ChatAdapter(this,messagesDAO);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        chatmanager = ChatManager.getInstanceFor(XMPPManager.connection);

        try {
            String localpart = JidUtil.validateBareJid(userId).getLocalpart();
            User user = userDAO.getUser(localpart);
            setTitle(user.getName());

        } catch (JidUtil.NotABareJidStringException e) {
            e.printStackTrace();
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }



        String meJID = XMPPManager.meID + "@localhost";
        final Subscription subscription = storIOSQLite
                .get()
                .listOfObjects(MessageEntity.class)
                .withQuery(RawQuery.builder()
                        .query("SELECT * FROM messages JOIN users ON (messages.userId = users.id) where messages.recipientBareJID = ?")
                        .observesTables("messages")
                        .args(userId)
                        .build())
                .withGetResolver(new MessageWithUserGetResolver())
                .prepare()
                .createObservable()
                .observeOn(mainThread())
                .subscribe(new Action1<List<MessageEntity>>() {
                    @Override
                    public void call(List<MessageEntity> messageEntities) {
                        adapter.setMessageEntities(messageEntities);
                        recyclerView.smoothScrollToPosition(messageEntities.size());

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        // In cases when you are not sure that query will be successful
                        // You can prevent crash of the application via error handler

                    }
                });

        chat = chatmanager.createChat(userId);

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String text = editText.getText().toString();
                if (text.equals("")) {
                    return false;
                }
                editText.setText("");

                try {
                    chat.sendMessage(text);

                    MessageEntity m = new MessageEntity();
                    m.setUserId(XMPPManager.meID);
                    m.setBody(text);
                    m.setType("chat");
                    m.setComponent("localhost");
                    m.setBareJid(XMPPManager.meID + "@localhost");
                    m.setFullJid(XMPPManager.meID + "@localhost");
                    m.setToJid(userId);
                    m.setRecipientBareJID(userId);

                    messagesDAO.insert(m);

                } catch (SmackException.NotConnectedException e) {
                    Log.d("AsyncTask", e.toString());
                }

                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
