package com.rvslabs.mebble.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.activities.groups.CreateGroupActivity;
import com.rvslabs.mebble.activities.login.StartActivity;
import com.rvslabs.mebble.activities.rooms.CreateRoomActivity;
import com.rvslabs.mebble.activities.rooms.SearchRoomActivity;
import com.rvslabs.mebble.activities.settings.SettingsActivity;
import com.rvslabs.mebble.adapter.MainPageAdapter;
import com.rvslabs.mebble.model.XMPPManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;
    @Bind(R.id.viewpager)
    ViewPager viewPager;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayoutr;
    @Bind(R.id.nvView)
    NavigationView nvView;

    XMPPManager xmppManager;
    private MainPageAdapter mainPageAdapter;
    private ActionBarDrawerToggle drawerToggle;
    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        setupDrawerContent(nvView);

        drawerToggle = setupDrawerToggle();
        mDrawerLayoutr.setDrawerListener(drawerToggle);

        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(this);
        boolean verified = pref.getBoolean("verified", false);

        if (!verified) {
            Intent i = new Intent(getApplicationContext(), StartActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            xmppManager = XMPPManager.getInstance();
            xmppManager.connectToXMPP();
        }

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mainPageAdapter = new MainPageAdapter(getSupportFragmentManager(),
                MainActivity.this);
        viewPager.setAdapter(mainPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Nullable
    @Override
    public ActionBarDrawerToggle.Delegate getDrawerToggleDelegate() {
        return super.getDrawerToggleDelegate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        TODO : Algumas funcoes comentadas para futura implementação
        switch (item.getItemId()) {
            case R.id.menu_create_room: {
                Intent intent = new Intent(this, CreateRoomActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.menu_search_room: {
                Intent i = new Intent(this, SearchRoomActivity.class);
                startActivity(i);
                return true;
            }
            case R.id.menu_create_chat: {
                Toast.makeText(MainActivity.this, "Create Chat", Toast.LENGTH_SHORT).show();
                return true;
            }
//            case R.id.menu_search_conversation: {
//                Toast.makeText(MainActivity.this, "Search Conversatoin", Toast.LENGTH_SHORT).show();
//                return true;
//            }
            case R.id.menu_create_conversation: {
                Toast.makeText(MainActivity.this, "Create Conversation", Toast.LENGTH_SHORT).show();
                return true;
            }
            case R.id.menu_create_group: {
                Intent i = new Intent(this, CreateGroupActivity.class);
                startActivity(i);
                Toast.makeText(MainActivity.this, "Create Group", Toast.LENGTH_SHORT).show();
                return true;
            }
//            case R.id.menu_search_contact:{
//                Toast.makeText(MainActivity.this, "Search Contact", Toast.LENGTH_SHORT).show();
//                return true;
//            }
//            case R.id.menu_create_contact:{
//                Toast.makeText(MainActivity.this, "Create Contact", Toast.LENGTH_SHORT).show();
//                return true;
//            }
            case R.id.home:
                mDrawerLayoutr.openDrawer(GravityCompat.START);
                return true;
        }

        return false;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.nav_rooms:
                viewPager.setCurrentItem(0);
                break;
            case R.id.nav_notifications:
                break;
            case R.id.nav_talks:
                viewPager.setCurrentItem(1);
                break;
            case R.id.nav_contacts:
                viewPager.setCurrentItem(2);
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;

            default:
        }

//        try {
//            fragment = (Fragment) fragmentClass.newInstance();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // Insert the fragment by replacing any existing fragment
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.sliding_tabs, fragment).commit();

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
//        setTitle(menuItem.getTitle());
        mDrawerLayoutr.closeDrawers();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawerLayoutr, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }
}