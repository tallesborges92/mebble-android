package com.rvslabs.mebble.activities.groups;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.util.Constants;
import com.rvslabs.mebble.util.Util;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateGroupActivity extends AppCompatActivity {

    public static final int PHOTO_SELECTED = 666;

    @Bind(R.id.activity_create_group_image)
    ImageView imageView;
    private String imageUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.activity_create_group_floatBtn)
    public void addPhotoPressed() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PHOTO_SELECTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case PHOTO_SELECTED: {

                Bitmap bitmap = Util.getBitmapFromURI(this, data);
                Bitmap resizedImage = Util.resizeImage(bitmap);

                try {
                    File fileResized = Util.createFile(resizedImage);

                    Util.uploadFile(fileResized, Constants.BUCKET_NAME_GROUPS, UUID.randomUUID().toString() + ".jpg", new Util.UploadToAmazonListener() {
                        @Override
                        public void uploadCompleted(String url) {
                            imageUrl = url;
                            Toast.makeText(CreateGroupActivity.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void uploadError() {
                            Toast.makeText(CreateGroupActivity.this, "Upload Error", Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (IOException e) {
                    Toast.makeText(CreateGroupActivity.this, "Error uploading image", Toast.LENGTH_SHORT).show();
                }

                imageView.setImageBitmap(resizedImage);

                break;
            }
        }
    }
}
