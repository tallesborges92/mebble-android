package com.rvslabs.mebble.activities.rooms;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.adapter.PlaceAutocompleteAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChooseLocalActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public static final int MAX_RANGE = 2000;
    public static final int MIN_RANGE = 100;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Bind(R.id.fragment_choose_local_actv)
    AutoCompleteTextView autoCompleteTextView;

    @Bind(R.id.seekBar)
    SeekBar seekBar;

    @Bind(R.id.textView12)
    TextView textViewRange;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private PlaceAutocompleteAdapter mAdapter;
    private GoogleApiClient mGoogleApiClient;
    public static final LatLng LAT_LNG = new LatLng(-25.42028,
            -49.268715);
    private Circle circle;
    private LatLng latLng;
    private String placeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_local);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setUpMapIfNeeded();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        LatLng latLng1 = SphericalUtil.computeOffset(LAT_LNG, 5000, 225);
        LatLng latLng2 = SphericalUtil.computeOffset(LAT_LNG, 5000, 45);

        mAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, new LatLngBounds(latLng1, latLng2), null);
        autoCompleteTextView.setAdapter(mAdapter);
        autoCompleteTextView.setOnItemClickListener(mAutocompleteClickListener);

        seekBar.setMax(MAX_RANGE);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int radius = progress + MIN_RANGE;
                if (circle != null) {
                    circle.setRadius(radius);
                }
                textViewRange.setText("Raio de Alcance : " + radius);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
        if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(LAT_LNG);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
        }

    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_choose_local, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                if (placeName != null) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("name", placeName);
                    resultIntent.putExtra("latLng", latLng);
                    resultIntent.putExtra("placeId", placeId);
                    resultIntent.putExtra("address", placeAddress);
                    resultIntent.putExtra("range", getRange());

                    setResult(Activity.RESULT_OK, resultIntent);
                }

                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each PlaceDTO suggestion in a PlaceAutocomplete object from which we
             read the place ID.


             */
            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);

            /*
             Issue a request to the Places Geo Data API to retrieve a PlaceDTO object with additional
              details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + item.description,
                    Toast.LENGTH_SHORT).show();
        }
    };

    private String placeName;
    private String placeAddress;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                places.release();
                return;
            }
            // Get the PlaceDTO object from the buffer.
            Place place = places.get(0);


            placeId = place.getId();
            latLng = place.getLatLng();
            placeName = place.getName().toString();
            placeAddress = place.getAddress().toString();
            mMap.addMarker(new MarkerOptions()
                    .title(placeName)
                    .snippet(placeAddress)
                    .position(latLng));

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

            CircleOptions circleOptions;
            circleOptions = new CircleOptions().center(latLng).radius(getRange());
            circleOptions.fillColor(Color.argb(100, 0, 0, 0));
            circleOptions.strokeWidth(2);

            circle = mMap.addCircle(circleOptions);

            places.release();
        }
    };

    private int getRange() {
        return seekBar.getProgress() + MIN_RANGE;
    }
}

