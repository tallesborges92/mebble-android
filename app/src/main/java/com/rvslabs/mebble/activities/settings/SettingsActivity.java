package com.rvslabs.mebble.activities.settings;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.rvslabs.mebble.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        setTitle("Ajustes");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.activity_settings_ll_my_perfil)
    public void myPerfilPressed() {
        Intent i = new Intent(this, MyProfileActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.activity_settings_ll_facebook_connect)
    public void connectWithFacebookPressed() {

    }

    @OnClick(R.id.activity_settings_ll_notifications)
    public void notificationsPressed() {
        Intent i = new Intent(this, NotificationSettingsActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.activity_settings_ll_chat_settings)
    public void chatSettingsPressed() {
        Intent i = new Intent(this, ChatSettingsActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.activity_settings_ll_system_status)
    public void systemStatusPressed() {

    }

    @OnClick(R.id.activity_settings_ll_about_app)
    public void aboutAppPressed() {
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.activity_settings_btn_invite_friend)
    public void inviteFriendPressed() {
        Intent i = new Intent(this, InviteFriendActivity.class);
        startActivity(i);
    }
}
