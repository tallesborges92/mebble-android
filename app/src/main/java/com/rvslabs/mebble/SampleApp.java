package com.rvslabs.mebble;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDexApplication;

import com.rvslabs.mebble.model.db.DbModule;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class SampleApp extends MultiDexApplication {

    @Nullable
    private volatile AppComponent appComponent;

    private static Context context;

    public static Context getAppContext() {
        return SampleApp.context;
    }

    // Monitors Memory Leaks, because why not!
    // You can play with sample app and Rx subscriptions
    // To see how it can leak memory if you won't unsubscribe.
    @NonNull
    private RefWatcher refWatcher;

    @NonNull
    public static SampleApp get(@NonNull Context context) {
        return (SampleApp) context.getApplicationContext();
    }

    @NonNull
    public static AppComponent getAppComponent() {
        return get(getAppContext().getApplicationContext()).appComponent();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        refWatcher = LeakCanary.install(this);
        Timber.plant(new Timber.DebugTree());
        SampleApp.context = getApplicationContext();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Semibold.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    @NonNull
    private AppComponent appComponent() {
        if (appComponent == null) {
            synchronized (SampleApp.class) {
                if (appComponent == null) {
                    appComponent = createAppComponent();
                }
            }
        }

        //noinspection ConstantConditions
        return appComponent;
    }

    @NonNull
    private AppComponent createAppComponent() {
        return DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .dbModule(new DbModule())
                .build();
    }

    @NonNull
    public RefWatcher refWatcher() {
        return refWatcher;
    }
}
