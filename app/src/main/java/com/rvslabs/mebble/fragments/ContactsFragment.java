package com.rvslabs.mebble.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pushtorefresh.storio.contentresolver.StorIOContentResolver;
import com.pushtorefresh.storio.contentresolver.queries.Query;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.activities.ChatActivity;
import com.rvslabs.mebble.adapter.ContactsCursorAdapter;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.User;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContactsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "ContactsFragment";

    @Bind(R.id.fragment_contacts_lv)
    ListView listView;

    @Inject
    StorIOContentResolver storIOContentResolver;

    CursorAdapter adapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactsFragment.
     */
    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        ButterKnife.bind(this, view);

        SampleApp.getAppComponent().inject(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupCursorAdapter();
        XMPPManager.getInstance().syncronizedContactsToUsers();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts, menu);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // Gets a CursorAdapter
        setupCursorAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupCursorAdapter() {
//        String[] uiBindFrom = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
//                ContactsContract.Contacts.PHOTO_URI};
//        int[] uiBindTo = {R.id.row_contact_tv_name, R.id.imageView6};

        final Cursor contactsCursor = storIOContentResolver
                .get()
                .cursor()
                .withQuery(Query.builder()
                        .uri(ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
                        .sortOrder(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")
                        .build())
                .prepare()
                .executeAsBlocking();

        adapter = new ContactsCursorAdapter(getContext(), contactsCursor, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Get the Cursor
        Cursor cursor = adapter.getCursor();
        // Move to the selected contact
        cursor.moveToPosition(position);

        int indexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int indexContactId = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity._ID);

        String name = cursor.getString(indexName);
        String number = cursor.getString(indexNumber);
        String contactId = cursor.getString(indexContactId);

        Log.i(TAG, "onItemClick " + name);
        Log.i(TAG, "COntact ID: " + contactId);

        User user = XMPPManager.getInstance().sendValidNumber(number);

        if (user != null) {
            String userId = user.getId() + "@localhost";

            Intent i = new Intent(getContext(), ChatActivity.class);
            i.putExtra("userId", userId);
            startActivity(i); // brings up the second activity

            Toast.makeText(getContext(), "number: " + number, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "nope", Toast.LENGTH_SHORT).show();
        }

    }


}
