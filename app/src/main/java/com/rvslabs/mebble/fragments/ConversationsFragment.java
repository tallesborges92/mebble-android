package com.rvslabs.mebble.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pushtorefresh.storio.sqlite.Changes;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;
import com.rvslabs.mebble.R;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.activities.ChatActivity;
import com.rvslabs.mebble.activities.rooms.RoomChatActivity;
import com.rvslabs.mebble.adapter.ConversationsAdapter;
import com.rvslabs.mebble.model.db.Group;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.MessageWithUserGetResolver;
import com.rvslabs.mebble.model.db.dao.GroupDAO;
import com.rvslabs.mebble.model.db.dao.UserDAO;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

import static rx.android.schedulers.AndroidSchedulers.mainThread;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConversationsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConversationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConversationsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "ConversationsFragment";

    @Bind(R.id.fragment_conversations_lv)
    ListView listView;

    private ConversationsAdapter adapter;

    @Inject
    StorIOSQLite storIOSQLite;
    @Inject
    UserDAO userDao;
    @Inject
    GroupDAO groupDao;

    public static ConversationsFragment newInstance() {
        ConversationsFragment fragment = new ConversationsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ConversationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        adapter = new ConversationsAdapter(getContext(), 0);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        setupStorio();

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_conversations, menu);
    }

    private void setupStorio() {
        final Subscription subscription = storIOSQLite
                .get()
                .listOfObjects(MessageEntity.class)
                .withQuery(RawQuery.builder()
                                .query("SELECT *, (select count(mr.recipientbarejid) from messages as mr where mr.recipientbarejid = messages.recipientbarejid and mr.read = 0 group by mr.recipientbarejid) as notRead FROM messages left JOIN users ON (messages.userId = users.id or messages.component = 'groupchat.localhost') where messages.component = 'localhost' or messages.component = 'groupchat.localhost'group by messages.recipientBareJID")
                                .observesTables("messages")
                                .build()
                )
                .withGetResolver(new MessageWithUserGetResolver())
                .prepare()
                .createObservable()
                .observeOn(mainThread())
                .subscribe(new Action1<List<MessageEntity>>() {
                    @Override
                    public void call(List<MessageEntity> messageEntities) {
                        adapter.clear();
                        adapter.addAll(messageEntities);
                        adapter.notifyDataSetChanged();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e(TAG, "call " + throwable);
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_conversations, container, false);

        ButterKnife.bind(this, view);
        SampleApp.getAppComponent().inject(this);

        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        MessageEntity messageEntity = adapter.getItem(position);

        if (messageEntity.getComponent().equals("groupchat.localhost")) {
            String groupId = messageEntity.getBareJid();

            Group group = groupDao.getGroup(groupId);

            Intent i = new Intent(getContext(), RoomChatActivity.class);
            i.putExtra("multipleUsersModel", Parcels.wrap(group));
            startActivity(i);

        } else {
            String userId = messageEntity.getRecipientBareJID();

            Intent i = new Intent(getContext(), ChatActivity.class);
            i.putExtra("userId", userId);
            startActivity(i); // brings up the second activity
        }



    }

}
