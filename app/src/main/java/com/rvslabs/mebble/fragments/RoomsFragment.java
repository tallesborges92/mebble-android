package com.rvslabs.mebble.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.rvslabs.mebble.R;
import com.rvslabs.mebble.activities.rooms.RoomChatActivity;
import com.rvslabs.mebble.adapter.RoomsAdapter;
import com.rvslabs.mebble.model.GroupsUtil;
import com.rvslabs.mebble.model.Room;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.xmpp.RoomsIQCategory;

import org.parceler.Parcels;

import java.util.LinkedHashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RoomsFragment extends Fragment implements ExpandableListView.OnChildClickListener {
    private static final String TAG = "Rooms Fragment";
    @Bind(R.id.roomslistView)
    ExpandableListView roomsListView;

    private LinkedHashMap<RoomsIQCategory.Category, List<Room>> groups;

    private RoomsAdapter adapter;


    public static RoomsFragment newInstance() {
        return new RoomsFragment();
    }

    public RoomsFragment() {
        // Required empty public constructor
        groups = GroupsUtil.initGroups();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rooms, container, false);
        ButterKnife.bind(this, view);

        roomsListView.setGroupIndicator(null);
        adapter = new RoomsAdapter(getActivity(), groups);
        roomsListView.setAdapter(adapter);
        roomsListView.setOnChildClickListener(this);
        roomsListView.setDivider(null);
        roomsListView.setChildDivider(null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_rooms, menu);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                addRooms(XMPPManager.getRooms(RoomsIQCategory.Category.suggestion), RoomsIQCategory.Category.suggestion);
                addRooms(XMPPManager.getRooms(RoomsIQCategory.Category.created), RoomsIQCategory.Category.created);
                addRooms(XMPPManager.getRooms(RoomsIQCategory.Category.participating), RoomsIQCategory.Category.participating);

                return null;
            }
        }.execute();
    }

    public void addRooms(final List<Room> rooms, final RoomsIQCategory.Category category) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.addRoomsCategory(category, rooms);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent intent = new Intent(getActivity(), RoomChatActivity.class);

        RoomsIQCategory.Category category = (RoomsIQCategory.Category) groups.keySet().toArray()[groupPosition];
        List<Room> rooms = groups.get(category);
        Room room = rooms.get(childPosition);
        intent.putExtra("multipleUsersModel", Parcels.wrap(room));

        startActivity(intent);
        return false;
    }

}
