package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 21/09/15.
 */
public class UserIQSet extends UserIQ {

    private String name;
    private String image;

    public UserIQSet(String name, String image) {
        super();
        this.setType(Type.set);
        this.name = name;
        this.image = image;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.openElement("user");

        if (name != null) xml.element("name", name);
        if (image != null) xml.element("image", image);

        xml.closeElement("user");

        return xml;
    }
}
