package com.rvslabs.mebble.xmpp;

import com.rvslabs.mebble.model.db.Group;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by rvslabs on 06/10/15.
 */
public class GroupProvider extends ExtensionElementProvider<Group> {

    @Override
    public Group parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        Group group = new Group();

        outerloop: while (true) {
            switch (parser.next()) {
                case XmlPullParser.START_TAG:
                    switch (parser.getName()) {
                        case "group":
                            group.setName(parser.getAttributeValue("", "name"));
                            group.setImage(parser.getAttributeValue("", "image"));
                            group.setJID(parser.getAttributeValue("", "jid"));
                            break;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (parser.getDepth() == initialDepth) {
                        break outerloop;
                    }
                    break;
            }
        }

        return group;
    }
}
