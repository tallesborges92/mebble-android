package com.rvslabs.mebble.xmpp;

import com.rvslabs.mebble.model.Room;

import org.jivesoftware.smack.packet.IQ;

/**
 * Created by rvslabs on 03/09/15.
 */
public class RoomIQIn extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "mebble:room:in";

    public RoomIQIn(Room room) {
        super(ELEMENT,NAMESPACE);
        setTo(room.getJID());
        setType(Type.set);
    }


    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        return xml;
    }
}
