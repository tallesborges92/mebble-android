package com.rvslabs.mebble.xmpp;

import java.util.Locale;

/**
 * Created by rvslabs on 15/09/15.
 */
public class RoomsIQCategory extends RoomsIQ {

    private Category category;

    public RoomsIQCategory(Category category) {
        super();
        this.category = category;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.element("category", category.toString());

        return xml;
    }


    public enum Category {

        fixed,

        suggestion,

        participating,

        created;

        /**
         * Converts a String into the corresponding types. Valid String values
         * that can be converted to types are: "get", "set", "result", and "error".
         *
         * @param string the String value to covert.
         * @return the corresponding Type.
         * @throws IllegalArgumentException when not able to parse the string parameter
         * @throws NullPointerException     if the string is null
         */
        public static Type fromString(String string) {
            return Type.valueOf(string.toLowerCase(Locale.US));
        }


        public String getName() {
            switch (this) {
                case fixed:
                    return "Salas a minha volta";
                case suggestion:
                    return "Salas sugeridas";
                case participating:
                    return "Salas que participo";
                case created:
                    return "Salas criadas";
                default:
                    return "";
            }
        }
    }
}
