package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 28/09/15.
 */
public class MebbleErrorRoomExist extends MebbleError{

    public static String NAMESPACE = "mebble:error:room_exist";

    private String otherRoomId;

    public MebbleErrorRoomExist(String otherRoomId) {
        super(NAMESPACE);
        this.otherRoomId = otherRoomId;
    }

    public String getOtherRoomId() {
        return otherRoomId;
    }

    public void setOtherRoomId(String otherRoomId) {
        this.otherRoomId = otherRoomId;
    }
}
