package com.rvslabs.mebble.xmpp;

import com.rvslabs.mebble.model.Room;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

/**
 * Created by rvslabs on 27/08/15.
 */
public class RoomIQ extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "mebble:iq:room";

    private Room room;

    public RoomIQ(Room room) {
        super(ELEMENT, NAMESPACE);
        this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.optElement(new RoomItem(room));

        return xml;
    }

    private class RoomItem implements ExtensionElement {

        public static final String ELEMENT = "x";
        public static final String NAMESPACE = "mebble:x:room_form";

        private Room room;

        public RoomItem(Room room) {
            this.room = room;
        }

        @Override
        public String getElementName() {
            return ELEMENT;
        }

        @Override
        public String getNamespace() {
            return NAMESPACE;
        }

        /*
        <iq from="user4@localhost/web" type="set" id="10:web">
          <query xmlns="mebble:iq:room">
            <x xmlns="mebble:x:room_form">
              <name>qqqqq</name>
              <description>asdfasdf</description>
              <range>200</range>
              <latitude>-25.4915492681055</latitude>
              <longitude>-49.2465901076294</longitude>
              <addressDescription>created from user location</addressDescription>
              <urlImage>http://lorempixel.com/400/200/</urlImage>
            </x>
          </query>
        </iq>
         */
        @Override
        public CharSequence toXML() {
            XmlStringBuilder xml = new XmlStringBuilder(this);

            xml.rightAngleBracket();

            xml.optElement("name", room.getName());
            xml.optElement("description", room.getDescription());
            xml.optElement("range", String.valueOf(room.getRange()));
            xml.optElement("latitude", String.valueOf(room.getLatitude()));
            xml.optElement("longitude", String.valueOf(room.getLongitude()));
            xml.optElement("addressDescription", room.getAddress());
            xml.optElement("urlImage", room.getImageUrl());
            xml.optElement("placeId", room.getPlaceId());

            xml.closeElement(this);

            return xml;
        }
    }

}
