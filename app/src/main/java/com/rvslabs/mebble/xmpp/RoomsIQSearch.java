package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 15/09/15.
 */
public class RoomsIQSearch extends RoomsIQ {

    private String search;

    public RoomsIQSearch(String search) {
        super();
        this.search = search;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.element("search", search);

        return xml;
    }
}
