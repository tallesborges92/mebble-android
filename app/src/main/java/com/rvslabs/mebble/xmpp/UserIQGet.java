package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 21/09/15.
 */
public class UserIQGet extends UserIQ {

    private String value;
    private QueryType queryType;

    public UserIQGet(String value, QueryType queryType) {
        super();
        this.setType(Type.get);
        this.value = value;
        this.queryType = queryType;
    }


    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {

        switch (queryType) {
            case UserId:
                xml.attribute("type", "userId");
                break;
            case Number:
                xml.attribute("type", "number");
                break;
        }

        xml.attribute("value", value);
        xml.setEmptyElement();

        return xml;
    }

    public enum QueryType {
        UserId,
        Number
    }

}


