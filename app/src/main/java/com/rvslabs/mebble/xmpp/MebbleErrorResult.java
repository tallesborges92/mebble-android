package com.rvslabs.mebble.xmpp;

import org.jivesoftware.smack.packet.SimpleIQ;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 25/09/15.
 */
public class MebbleErrorResult extends SimpleIQ {

    public static final String ELEMENT = "x";
    public static final String NAMESPACE = "mebble:error";

    private List<MebbleError> errors;

    public MebbleErrorResult() {
        super(ELEMENT,NAMESPACE);
        setType(Type.error);
        errors = new ArrayList<>();
    }

    protected MebbleErrorResult(String childElementName, String childElementNamespace) {
        super(childElementName, childElementNamespace);
    }

    public List<MebbleError> getErrors() {
        return errors;
    }


}
