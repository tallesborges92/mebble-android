package com.rvslabs.mebble.xmpp;

import com.rvslabs.mebble.model.Room;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by rvslabs on 20/08/15.
 */
public class RoomsIQProvider extends IQProvider<RoomsIQ> {

    @Override
    public RoomsIQ parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        RoomsIQ roomsIQ = new RoomsIQ();

        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getName().equals("item")) {
                    Room room = new Room();
                    try {
                        room.setJID(parser.getAttributeValue("", "jid"));
                        room.setName(parser.getAttributeValue("", "name"));
                        room.setAddress(parser.getAttributeValue("", "address_description"));
                        room.setDescription(parser.getAttributeValue("", "description"));
                        room.setImageUrl(parser.getAttributeValue("", "url_image"));
                        room.setUsers(Integer.valueOf(parser.getAttributeValue("", "total_users")));
                        room.setRange(Integer.valueOf(parser.getAttributeValue("", "range")));
                        room.setLongitude(Float.valueOf(parser.getAttributeValue("", "longitude")));
                        room.setLatitude(Float.valueOf(parser.getAttributeValue("", "latitude")));
                    } catch (NumberFormatException ignored) {

                    } finally {
                        roomsIQ.getRooms().add(room);
                    }

                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("query")) {
                    done = true;
                }
            }
        }

        return roomsIQ;
    }
}
