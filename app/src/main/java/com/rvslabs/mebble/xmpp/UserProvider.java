package com.rvslabs.mebble.xmpp;


import com.rvslabs.mebble.model.db.User;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by rvslabs on 04/08/15.
 */
public class UserProvider extends ExtensionElementProvider<User> {
    @Override
    public User parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        User user = new User();

        outerloop:
        while (true) {
            switch (parser.next()) {
                case XmlPullParser.START_TAG:
                    switch (parser.getName()) {
                        case "item":
                            user.setName(parser.getAttributeValue("", "name"));
                            user.setImage(parser.getAttributeValue("", "image"));
                            user.setId(parser.getAttributeValue("","id"));
                            break;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (parser.getDepth() == initialDepth) {
                        break outerloop;
                    }
                    break;
            }
        }

        return user;
    }
}
