package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 28/09/15.
 */
public class MebbleError {

    private String NAMESPACE;

    private String errorDescription;

    public MebbleError(String NAMESPACE) {
        this.NAMESPACE = NAMESPACE;
    }

    public String getNAMESPACE() {
        return NAMESPACE;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
