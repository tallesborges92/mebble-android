package com.rvslabs.mebble.xmpp;


import com.rvslabs.mebble.model.db.User;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by rvslabs on 21/09/15.
 */
public class UserIQProvider extends IQProvider<UserIQ> {

    @Override
    public UserIQ parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {

        UserIQ userIQ = new UserIQ();

        outerloop:
        while (true) {
            switch (parser.next()) {
                case XmlPullParser.START_TAG:
                    switch (parser.getName()) {
                        case "item":
                            User user = new User();
                            user.setName(parser.getAttributeValue("", "name"));
                            user.setImage(parser.getAttributeValue("", "image"));
                            user.setId(parser.getAttributeValue("", "id"));
                            userIQ.setUser(user);
                            break;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    if (parser.getDepth() == initialDepth) {
                        break outerloop;
                    }
                    break;
            }
        }

        return userIQ;
    }

}
