package com.rvslabs.mebble.xmpp;


import com.rvslabs.mebble.model.db.User;

import org.jivesoftware.smack.packet.IQ;

/**
 * Created by rvslabs on 18/09/15.
 */
public class UserIQ extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "mebble:iq:user";

    private User user;

    public UserIQ() {
        super(ELEMENT, NAMESPACE);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        return null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
