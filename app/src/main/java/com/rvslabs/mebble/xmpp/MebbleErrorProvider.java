package com.rvslabs.mebble.xmpp;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by rvslabs on 25/09/15.
 */
public class MebbleErrorProvider extends IQProvider<MebbleErrorResult> {

    @Override
    public MebbleErrorResult parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {

        MebbleErrorResult mebbleErrorResult = new MebbleErrorResult();

        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                if (parser.getNamespace().equals(MebbleErrorRoomExist.NAMESPACE)) {

                    String otherRoomId = parser.getAttributeValue("", "otherRoomId");
                    String errorDescription = parser.getAttributeValue("", "errorDescription");

                    MebbleErrorRoomExist error = new MebbleErrorRoomExist(otherRoomId);
                    error.setErrorDescription(errorDescription);

                    mebbleErrorResult.getErrors().add(error);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("error")) {
                    done = true;
                }
            }
        }

        return mebbleErrorResult;
    }
}
