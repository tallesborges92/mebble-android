package com.rvslabs.mebble.xmpp;

import com.rvslabs.mebble.model.Room;

import org.jivesoftware.smack.packet.IQ;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rvslabs on 20/08/15.
 */
public class RoomsIQ extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "mebble:iq:room";

    private List<Room> rooms = new ArrayList<>();

    public RoomsIQ() {
        super(ELEMENT, NAMESPACE);
    }


    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        return null;
    }

}
