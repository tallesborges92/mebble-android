package com.rvslabs.mebble.xmpp;

import org.jivesoftware.smack.packet.SimpleIQ;

/**
 * Created by rvslabs on 16/09/15.
 */
public class UserLocationIQ extends SimpleIQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "mebble:user:location";

    String latitude;
    String longitude;

    public UserLocationIQ( String latitude, String longitude) {
        this();
        this.latitude = latitude;
        this.longitude = longitude;
        this.setType(Type.set);
    }

    public UserLocationIQ() {
        super(ELEMENT,NAMESPACE);
    }


    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.halfOpenElement("item");
        xml.attribute("lon", longitude);
        xml.attribute("lat", latitude);
        xml.closeEmptyElement();

        return xml;
    }
}
