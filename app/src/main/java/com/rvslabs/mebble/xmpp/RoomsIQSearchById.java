package com.rvslabs.mebble.xmpp;

/**
 * Created by rvslabs on 28/09/15.
 */
public class RoomsIQSearchById extends RoomsIQ {


    private String roomId;

    public RoomsIQSearchById(String roomId) {
        super();
        this.roomId = roomId;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();

        xml.element("roomId", roomId);

        return xml;
    }

}
