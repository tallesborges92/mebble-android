package com.rvslabs.mebble;

import com.rvslabs.mebble.activities.ChatActivity;
import com.rvslabs.mebble.activities.MainActivity;
import com.rvslabs.mebble.activities.OtherUserProfileActivity;
import com.rvslabs.mebble.activities.SearchContactActivity;
import com.rvslabs.mebble.activities.rooms.AboutRoomActivity;
import com.rvslabs.mebble.activities.rooms.RoomChatActivity;
import com.rvslabs.mebble.adapter.ConversationsAdapter;
import com.rvslabs.mebble.fragments.ContactsFragment;
import com.rvslabs.mebble.fragments.ConversationsFragment;
import com.rvslabs.mebble.fragments.RoomsFragment;
import com.rvslabs.mebble.model.XMPPManager;
import com.rvslabs.mebble.model.db.DbModule;
import com.rvslabs.mebble.model.db.RoomUserGetResolver;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                AppModule.class,
                DbModule.class
        }
)
public interface AppComponent {

    void inject(RoomChatActivity roomActivity);

    void inject(XMPPManager xmppManager);

    void inject(AboutRoomActivity roomActivity);

    void inject(RoomUserGetResolver roomUserGetResolver);

    void inject(MainActivity mainActivity);

    void inject(RoomsFragment roomsFragment);

    void inject(ChatActivity userChatActivity);

    void inject(ConversationsFragment conversationsFragment);

    void inject(ConversationsAdapter conversationsAdapter);

    void inject(OtherUserProfileActivity otherUserProfileActivity);

    void inject(ContactsFragment contactsFragment);

    void inject(SearchContactActivity searchContactActivity);
}
