
package com.rvslabs.mebble.model.api.mebble;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Country {

    @Expose
    private String alpha2Code;
    @Expose
    private String alpha3Code;
    @Expose
    private String callingCode;
    @Expose
    private String name;

    /**
     * 
     * @return
     *     The alpha2Code
     */
    public String getAlpha2Code() {
        return alpha2Code;
    }

    /**
     * 
     * @param alpha2Code
     *     The alpha2Code
     */
    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    /**
     * 
     * @return
     *     The alpha3Code
     */
    public String getAlpha3Code() {
        return alpha3Code;
    }

    /**
     * 
     * @param alpha3Code
     *     The alpha3Code
     */
    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    /**
     * 
     * @return
     *     The callingCode
     */
    public String getCallingCode() {
        return callingCode;
    }

    /**
     * 
     * @param callingCode
     *     The callingCode
     */
    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
