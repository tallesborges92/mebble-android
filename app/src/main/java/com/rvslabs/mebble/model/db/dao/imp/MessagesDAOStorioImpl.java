package com.rvslabs.mebble.model.db.dao.imp;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;

/**
 * Created by rvslabs on 13/10/15.
 */
public class MessagesDAOStorioImpl implements MessagesDAO {

    StorIOSQLite storIOSQLite;

    public MessagesDAOStorioImpl(@NonNull StorIOSQLite storIOSQLite) {
        this.storIOSQLite = storIOSQLite;
    }


    @Override
    public PutResult insert(MessageEntity messageEntity) {
        messageEntity.setDate(System.currentTimeMillis());
        messageEntity.setRead(false);

        return insertOrSave(messageEntity);
    }

    @Override
    public PutResult save(MessageEntity messageEntity) {
        return insertOrSave(messageEntity);
    }

    private PutResult insertOrSave(MessageEntity messageEntity) {
        return storIOSQLite
                .put()
                .object(messageEntity)
                .prepare()
                .executeAsBlocking();
    }
}
