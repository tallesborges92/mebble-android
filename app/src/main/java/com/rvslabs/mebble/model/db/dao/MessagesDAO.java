package com.rvslabs.mebble.model.db.dao;

import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.rvslabs.mebble.model.db.MessageEntity;

/**
 * Created by rvslabs on 13/10/15.
 */
public interface MessagesDAO {

    PutResult insert(MessageEntity messageEntity);

    PutResult save(MessageEntity messageEntity);


}
