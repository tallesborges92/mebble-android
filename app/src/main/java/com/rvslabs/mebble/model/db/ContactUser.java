package com.rvslabs.mebble.model.db;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

/**
 * Created by rvslabs on 08/10/15.
 */
@StorIOSQLiteType(table = "contact_user")
public class ContactUser {

    @StorIOSQLiteColumn(name = "id")
    String contactId;

    @StorIOSQLiteColumn(name = "name", key = true)
    String contactName;

    @StorIOSQLiteColumn(name = "user_id")
    String userId;

    public ContactUser() {
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
