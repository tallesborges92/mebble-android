package com.rvslabs.mebble.model.db;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;

/**
 * Created by rvslabs on 18/08/15.
 */
public class RoomUserGetResolver extends DefaultGetResolver<RoomUserEntity> {

    @NonNull
    @Override
    public RoomUserEntity mapFromCursor(Cursor cursor) {
        RoomUserEntity roomUserEntity = new RoomUserEntity();
        roomUserEntity.id = cursor.getString(cursor.getColumnIndex("id"));
        roomUserEntity.userId = cursor.getString(cursor.getColumnIndex("userId"));
        roomUserEntity.roomId = cursor.getString(cursor.getColumnIndex("roomId"));

        User userEntity = new User();
        userEntity.setId(roomUserEntity.userId);
        userEntity.setName(cursor.getString(cursor.getColumnIndex("name")));
        userEntity.setImage(cursor.getString(cursor.getColumnIndex("image")));

        roomUserEntity.userEntity = userEntity;

        return roomUserEntity;
    }
}
