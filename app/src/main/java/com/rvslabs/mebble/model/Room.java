package com.rvslabs.mebble.model;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.parceler.Parcel;


@Parcel
@StorIOSQLiteType(table = "rooms")
public class Room implements MultipleUsersModel {

    @StorIOSQLiteColumn(name = "jid", key = true)
    String jid;
    @StorIOSQLiteColumn(name = "name")
    String name;
    String description;
    String address;
    @StorIOSQLiteColumn(name = "image")
    String imageUrl;
    String placeId;
    float distance;
    int range;
    Float latitude;
    Float longitude;
    int users;

    public Room() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJID() {
        return jid;
    }

    @Override
    public String getImage() {
        return imageUrl;
    }

    public void setJID(String id) {
        this.jid = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getDistance() {
        return distance;
    }

    public int getUsers() {
        return users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public void setUsers(int users) {
        this.users = users;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
