package com.rvslabs.mebble.model;

import com.rvslabs.mebble.xmpp.RoomsIQCategory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by rvslabs on 03/08/15.
 */
public class GroupsUtil {


    public static LinkedHashMap<RoomsIQCategory.Category, List<Room>> initGroups() {

        LinkedHashMap<RoomsIQCategory.Category, List<Room>> groups = new LinkedHashMap<>();

        List<Room> fixedRooms = new ArrayList<>();

        Room nearby = new Room();
        nearby.setJID("nearby@userchat.localhost");
        nearby.setName("Próximo a você");
        nearby.setAddress("");
        nearby.setUsers(10);

        Room region = new Room();
        region.setJID("region@userchat.localhost");
        region.setName("Nesta região");
        region.setAddress("");
        region.setUsers(20);

        Room city = new Room();
        city.setName("Na cidade");
        city.setJID("");
        city.setAddress("");
        city.setUsers(30);

        fixedRooms.add(nearby);
        fixedRooms.add(region);
        fixedRooms.add(city);

        List<Room> myRooms = new ArrayList<>();

        List<Room> participatingRooms = new ArrayList<>();

        List<Room> suggestedRooms = new ArrayList<>();

        groups.put(RoomsIQCategory.Category.fixed, fixedRooms);
        groups.put(RoomsIQCategory.Category.created, myRooms);
        groups.put(RoomsIQCategory.Category.participating, participatingRooms);
        groups.put(RoomsIQCategory.Category.suggestion, suggestedRooms);

        return groups;
    }
}
