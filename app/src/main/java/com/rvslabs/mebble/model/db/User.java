package com.rvslabs.mebble.model.db;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.util.XmlStringBuilder;

/**
 * Created by rvslabs on 14/08/15.
 */
@StorIOSQLiteType(table = "users")
public class User implements ExtensionElement {


    public static final String ELEMENT = "x";
    public static final String NAMESPACE = "mebble:user";


    @StorIOSQLiteColumn(name = "id", key = true)
     String id;

    @StorIOSQLiteColumn(name = "name")
     String name;

    @StorIOSQLiteColumn(name = "image")
     String image;

    public User() {

    }

    public User(String id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public CharSequence toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.rightAngleBracket();
        xml.halfOpenElement("item");
        xml.optAttribute("name", getName());
        xml.optAttribute("image", getImage());
        xml.closeEmptyElement();
        xml.closeElement(this);
        return xml;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
