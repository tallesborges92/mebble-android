package com.rvslabs.mebble.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

/**
 * Created by rvslabs on 11/08/15.
 */
public class DbOpenHelper extends SQLiteOpenHelper {

    public DbOpenHelper(@NonNull Context context) {
        super(context, "mebble_db", null, 24);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        executeCreateTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS messages");
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS room_users");
        db.execSQL("DROP TABLE IF EXISTS groups");
        db.execSQL("DROP TABLE IF EXISTS contact_user");

        executeCreateTables(db);
    }

    private void executeCreateTables(SQLiteDatabase db) {
        db.execSQL(getCreateMessagesTableQuery());
        db.execSQL(getCreateUserTableQuery());
        db.execSQL(getCreateRoomUsersTableQuery());
        db.execSQL(getCreateRoomTableQuery());
        db.execSQL(getCreateContactUserTableQuery());
    }

    @NonNull
    private static String getCreateMessagesTableQuery() {
        return "CREATE TABLE messages (_id INTEGER PRIMARY KEY  AUTOINCREMENT, barejid TEXT, fulljid TEXT, type TEXT, body TEXT, userId TEXT, toJid TEXT, recipientBareJID TEXT, component TEXT, date NUMERIC,read NUMERIC)";
    }

    @NonNull
    private static String getCreateUserTableQuery() {
        return "CREATE TABLE users (id TEXT PRIMARY KEY, name TEXT, image TEXT)";
    }

    @NonNull
    private static String getCreateRoomUsersTableQuery() {
        return "CREATE TABLE room_users (id TEXT PRIMARY KEY, roomId TEXT, userId TEXT)";
    }

    @NonNull
    private static String getCreateRoomTableQuery() {
        return "CREATE TABLE groups (jid TEXT PRIMARY KEY, name TEXT, image TEXT)";
    }

    private static String getCreateContactUserTableQuery() {
        return "CREATE TABLE contact_user (id TEXT PRIMARY KEY, name TEXT, user_id TEXT)";
    }
}
