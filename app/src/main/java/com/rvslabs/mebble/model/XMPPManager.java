package com.rvslabs.mebble.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;

import com.pushtorefresh.storio.contentresolver.StorIOContentResolver;
import com.pushtorefresh.storio.contentresolver.queries.Query;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.rvslabs.mebble.SampleApp;
import com.rvslabs.mebble.exceptions.MebbleErrorException;
import com.rvslabs.mebble.model.db.ContactUser;
import com.rvslabs.mebble.model.db.Group;
import com.rvslabs.mebble.model.db.MessageEntity;
import com.rvslabs.mebble.model.db.RoomUserEntity;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;
import com.rvslabs.mebble.util.LocationManager;
import com.rvslabs.mebble.xmpp.GroupProvider;
import com.rvslabs.mebble.xmpp.MebbleErrorProvider;
import com.rvslabs.mebble.xmpp.MebbleErrorResult;
import com.rvslabs.mebble.xmpp.RoomIQ;
import com.rvslabs.mebble.xmpp.RoomIQIn;
import com.rvslabs.mebble.xmpp.RoomsIQ;
import com.rvslabs.mebble.xmpp.RoomsIQCategory;
import com.rvslabs.mebble.xmpp.RoomsIQProvider;
import com.rvslabs.mebble.xmpp.RoomsIQSearch;
import com.rvslabs.mebble.xmpp.RoomsIQSearchById;
import com.rvslabs.mebble.xmpp.UserIQ;
import com.rvslabs.mebble.xmpp.UserIQGet;
import com.rvslabs.mebble.xmpp.UserIQProvider;
import com.rvslabs.mebble.xmpp.UserIQSet;
import com.rvslabs.mebble.xmpp.UserLocationIQ;
import com.rvslabs.mebble.xmpp.UserProvider;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.StanzaIdFilter;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by rvslabs on 03/08/15.
 */
public class XMPPManager {

    private static final String TAG = "XMPPManager";

    public static XMPPTCPConnection connection;

    private static XMPPManager instance = null;

    private static LocationManager locationManager;

    public static String meID = "user1";
    public static String mePWD = "user1";

    @Inject
    StorIOSQLite storIOSQLite;
    @Inject
    StorIOContentResolver storIOContentResolver;
    @Inject
    MessagesDAO messagesDAO;


    public static XMPPManager getInstance() {
        if (instance == null) {
            instance = new XMPPManager();
//            instance.resetSharedPreferences(SampleApp.getAppContext());
        }
        return instance;
    }


    public void resetSharedPreferences(Context context) {
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().clear().apply();
    }


    XMPPManager() {
        SampleApp.getAppComponent().inject(this);
        locationManager = new LocationManager(SampleApp.getAppContext());
    }

    public static String getUserNumber() {
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(SampleApp.getAppContext());
        String ddi = pref.getString("ddi", "");
        String number = pref.getString("number", "");

        return "+" + ddi + " " + number;
    }

    public void connectToXMPP() {

        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(SampleApp.getAppContext());

        meID = pref.getString("id", "user1");
        mePWD = pref.getString("password", "user1");

        AsyncTask<Void, Void, Void> connectionThread = new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... params) {
                try {
                    // Create a connection to the jabber.org server.

                    XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                            .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                            .setUsernameAndPassword(meID, mePWD)
                            .setServiceName("localhost")
                            .setHost("192.168.100.234")
                            .setPort(5222)
                            .setDebuggerEnabled(true)
                            .build();

                    connection = new XMPPTCPConnection(config);
                    connection.addConnectionListener(new ConnectionListener() {
                        @Override
                        public void connected(XMPPConnection connection) {
                            Log.d(TAG, "connected() called with: " + "connection = [" + connection + "]");
                        }

                        @Override
                        public void authenticated(XMPPConnection connection, boolean resumed) {
                            Log.d(TAG, "authenticated() called with: " + "connection = [" + connection + "], resumed = [" + resumed + "]");
                            syncronizedContactsToUsers();
                        }

                        @Override
                        public void connectionClosed() {
                            Log.d(TAG, "connectionClosed() called with: " + "");
                        }

                        @Override
                        public void connectionClosedOnError(Exception e) {
                            Log.d(TAG, "connectionClosedOnError() called with: " + "e = [" + e + "]");
                        }

                        @Override
                        public void reconnectionSuccessful() {
                            Log.d(TAG, "reconnectionSuccessful() called with: " + "");
                        }

                        @Override
                        public void reconnectingIn(int seconds) {
                            Log.d(TAG, "reconnectingIn() called with: " + "seconds = [" + seconds + "]");
                        }

                        @Override
                        public void reconnectionFailed(Exception e) {
                            Log.d(TAG, "reconnectionFailed() called with: " + "e = [" + e + "]");
                        }
                    });

                    connection.addAsyncStanzaListener(new ProcessMessage(), new AndFilter(StanzaTypeFilter.MESSAGE));
                    connection.addAsyncStanzaListener(new ProcessPresence(), new AndFilter(StanzaTypeFilter.PRESENCE));

                    ProviderManager.addExtensionProvider(User.ELEMENT, User.NAMESPACE, new UserProvider());
                    ProviderManager.addExtensionProvider(Group.ELEMENT, Group.NAMESPACE, new GroupProvider());

                    ProviderManager.addIQProvider(RoomsIQ.ELEMENT, RoomsIQ.NAMESPACE, new RoomsIQProvider());

                    ProviderManager.addIQProvider(UserIQ.ELEMENT, UserIQ.NAMESPACE, new UserIQProvider());

                    ProviderManager.addIQProvider(MebbleErrorResult.ELEMENT, MebbleErrorResult.NAMESPACE, new MebbleErrorProvider());

                    connection.connect();
                    connection.login();
                } catch (SmackException | IOException | XMPPException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error Connecting", e);
                }

                return null;
            }
        };

        connectionThread.execute();

    }

    public static Room createRoom(Room room) throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException, MebbleErrorException {
        RoomIQ roomIQ = new RoomIQ(room);
        roomIQ.setType(IQ.Type.set);

        Stanza result = connection.createPacketCollectorAndSend(roomIQ).nextResultOrThrow();

        if (result instanceof MebbleErrorResult) {
            throw new MebbleErrorException((MebbleErrorResult) result);
        }

        RoomsIQ resultOrThrow = (RoomsIQ) result;

        return resultOrThrow.getRooms().get(0);
    }

    public static void sendInRoom(Room room) {
        RoomIQIn roomIQIn = new RoomIQIn(room);

        try {
            connection.createPacketCollectorAndSend(roomIQIn);
        } catch (SmackException.NotConnectedException e) {
            Log.e(TAG, "sendInRoom " + e);
        }
    }

    public User sendValidNumber(String number) {

        UserIQ get = new UserIQGet(number, UserIQGet.QueryType.Number);

        try {
            UserIQ response = connection.createPacketCollectorAndSend(get).nextResultOrThrow();

            if (response.getUser() != null) {
                User userEntity = new User(response.getUser().getId(), response.getUser().getName(), response.getUser().getImage());

                PutResult result = storIOSQLite
                        .put()
                        .object(userEntity)
                        .prepare()
                        .executeAsBlocking();
            }

            return response.getUser();
        } catch (SmackException.NotConnectedException | ClassCastException e) {
            Log.e(TAG, "sendInRoom " + e);
        } catch (XMPPException.XMPPErrorException | SmackException.NoResponseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateUserLocation(String latitude, String longitude) {
        UserLocationIQ userLocationIQ = new UserLocationIQ(latitude, longitude);
        try {
            if (connection != null && connection.isConnected()) {
                connection.createPacketCollectorAndSend(userLocationIQ);
            }
        } catch (SmackException.NotConnectedException e) {
            Log.e(TAG, "updateUserLocation fail to update");
        }
    }

    public static User getUserInfo(String user) {
        UserIQ get = new UserIQGet(user, UserIQGet.QueryType.UserId);

        User userModel = null;
        try {
            UserIQ result = connection.createPacketCollectorAndSend(get).nextResultOrThrow();

            userModel = result.getUser();

        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
            Log.e(TAG, "getUserInfo " + e);
            e.printStackTrace();
        }

        return userModel;
    }

    public static void setUserNameAndImage(String name, String image) throws SmackException.NotConnectedException {
        UserIQ set = new UserIQSet(name, image);
        connection.createPacketCollectorAndSend(set);
    }

    public static Room getRoomById(String roomId) throws MebbleErrorException {
        RoomsIQ xmppRoom = new RoomsIQSearchById(roomId);
        xmppRoom.setType(IQ.Type.get);

        try {
            RoomsIQ roomsManager = connection.createPacketCollectorAndSend(xmppRoom).nextResultOrThrow();
            List<Room> rooms = roomsManager.getRooms();
            if (rooms.size() == 0) {
                throw new MebbleErrorException(null);
            }
            Log.i(TAG, rooms.toString());
            return rooms.get(0);
        } catch (Exception e) {
            Log.e(TAG, null, e);
        }

        throw new MebbleErrorException(null);
    }

    public static List<Room> getRoomsByName(String name) {
        RoomsIQ xmppRoom = new RoomsIQSearch(name);
        xmppRoom.setType(IQ.Type.get);

        try {
            RoomsIQ roomsManager = connection.createPacketCollectorAndSend(xmppRoom).nextResultOrThrow();
            List<Room> rooms = roomsManager.getRooms();
            Log.i(TAG, rooms.toString());
            return rooms;
        } catch (Exception e) {
            Log.e(TAG, null, e);
        }

        return new ArrayList<>();
    }

    public static List<Room> getRooms(RoomsIQCategory.Category category) {
        RoomsIQ xmppRoom = new RoomsIQCategory(category);
        xmppRoom.setType(IQ.Type.get);

        try {
            RoomsIQ roomsManager = connection.createPacketCollectorAndSend(xmppRoom).nextResultOrThrow();
            List<Room> rooms = roomsManager.getRooms();
            Log.i(TAG, rooms.toString());
            return rooms;
        } catch (Exception e) {
            Log.e(TAG, null, e);
        }

        return new ArrayList<>();
    }

    private class ProcessMessage implements StanzaListener {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            Message message = (Message) packet;

            if (message.getBody() != null) {
                try {
                    Jid jid = JidCreate.from(message.getFrom());
                    String resourceOrNull = jid.getResourceOrNull();

                    if (resourceOrNull != null) {
                        MessageEntity m = new MessageEntity(message);

                        PutResult putResult = messagesDAO.insert(m);

                        Log.d(TAG, "PutResult: " + putResult);

                        Log.d("MessageListener", message.toString());
                    }

                } catch (XmppStringprepException e) {
                    Log.e(null, null, e);
                }
            } else {
                Group group = (Group) message.getExtension(Group.NAMESPACE);

                if (group != null) {

                    PutResult putResult = storIOSQLite
                            .put()
                            .object(group)
                            .prepare()
                            .executeAsBlocking();

                    Log.d(TAG, "PutResult: " + putResult);
                }

            }


            Log.i(TAG, packet.toXML().toString());
        }
    }

    private class ProcessPresence implements StanzaListener {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            Presence presence = (Presence) packet;

            try {
                Jid jid = JidCreate.from(presence.getFrom());
                String resourceOrNull = jid.getResourceOrNull();

                if (resourceOrNull != null) {

                    User user = presence.getExtension(User.ELEMENT, User.NAMESPACE);

                    if (user != null) {

                        PutResult result = storIOSQLite
                                .put()
                                .object(user)
                                .prepare()
                                .executeAsBlocking();

                        RoomUserEntity roomUserEntity = new RoomUserEntity(presence);

                        if (presence.getType().equals(Presence.Type.available)) {
                            storIOSQLite
                                    .put()
                                    .object(roomUserEntity)
                                    .prepare()
                                    .executeAsBlocking();
                        } else if (presence.getType().equals(Presence.Type.unavailable)) {
                            storIOSQLite
                                    .delete()
                                    .object(roomUserEntity)
                                    .prepare()
                                    .executeAsBlocking();
                        }

                        Log.i(TAG, result.toString());
                    }

                }


            } catch (XmppStringprepException e) {
                Log.e(null, null, e);
            }

            Log.i(TAG, packet.toXML().toString());
        }
    }

    public void syncronizedContactsToUsers() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                Cursor contactsCursor = storIOContentResolver
                        .get()
                        .cursor()
                        .withQuery(Query.builder()
                                .uri(ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
                                .build())
                        .prepare()
                        .executeAsBlocking();

                while (contactsCursor.moveToNext()) {

                    int indexName = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                    int indexNumber = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    int indexContactId = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Identity._ID);

                    String contactName = contactsCursor.getString(indexName);
                    String number = contactsCursor.getString(indexNumber);
                    String contactId = contactsCursor.getString(indexContactId);

                    User user = sendValidNumber(number);

                    if (user != null) {
                        storIOSQLite.put().object(user).prepare().executeAsBlocking();

                        ContactUser contactUser = new ContactUser();
                        contactUser.setUserId(user.getId());
                        contactUser.setContactId(contactId);
                        contactUser.setContactName(contactName);

                        storIOSQLite.put().object(contactUser).prepare().executeAsBlocking();
                    }

                }

                contactsCursor.close();

                return null;
            }
        }.execute();
    }

    public LocationManager getLocationManager() {
        return locationManager;
    }

    public boolean leaveConference(MultipleUsersModel conference) {
        Presence presence = new Presence(Presence.Type.unavailable);
        presence.setTo(conference.getJID());
        presence.addExtension(new ExtensionElement() {
            @Override
            public String getNamespace() {
                return "mebble:room:left";
            }

            @Override
            public String getElementName() {
                return "x";
            }

            @Override
            public CharSequence toXML() {
                XmlStringBuilder xml = new XmlStringBuilder(this);
                xml.closeEmptyElement();
                return xml;
            }
        });

        try {
            Stanza stanza = connection.createPacketCollectorAndSend(new StanzaIdFilter(presence), presence).nextResultOrThrow();
            Log.d(TAG, "leaveConference() returned: " + stanza);
            return true;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        }
        return false;
    }
}
