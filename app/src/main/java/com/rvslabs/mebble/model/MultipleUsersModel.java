package com.rvslabs.mebble.model;

/**
 * Created by rvslabs on 07/10/15.
 */
public interface MultipleUsersModel {

    String getName();

    String getJID();

    String getImage();

}

