package com.rvslabs.mebble.model.db;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver;

/**
 * Created by rvslabs on 14/08/15.
 */
public class MessageWithUserGetResolver extends DefaultGetResolver<MessageEntity> {
    @NonNull
    @Override
    public MessageEntity mapFromCursor(@NonNull Cursor cursor) {
        MessageEntity object = new MessageEntity();

        object.id = cursor.getLong(cursor.getColumnIndex("_id"));
        object.fullJid = cursor.getString(cursor.getColumnIndex("fulljid"));
        object.body = cursor.getString(cursor.getColumnIndex("body"));
        object.type = cursor.getString(cursor.getColumnIndex("type"));
        object.bareJid = cursor.getString(cursor.getColumnIndex("barejid"));
        object.userId = cursor.getString(cursor.getColumnIndex("userId"));
        object.toJid = cursor.getString(cursor.getColumnIndex("toJid"));
        object.recipientBareJID = cursor.getString(cursor.getColumnIndex("recipientBareJID"));
        object.component = cursor.getString(cursor.getColumnIndex("component"));
        object.date = cursor.getLong(cursor.getColumnIndex("date"));
        object.read = cursor.getInt(cursor.getColumnIndex("read")) != 0;
        int columnIndex = cursor.getColumnIndex("notRead");
        if (columnIndex != -1) {
            object.totalNotReadMessages = cursor.getInt(columnIndex);
        }

        User userEntity = new User();
        userEntity.id = cursor.getString(cursor.getColumnIndex("id"));
        userEntity.name = cursor.getString(cursor.getColumnIndex("name"));
        userEntity.image = cursor.getString(cursor.getColumnIndex("image"));

        object.userEntity = userEntity;

        return object;
    }
}
