package com.rvslabs.mebble.model.db.dao;

import com.rvslabs.mebble.model.db.Group;

/**
 * Created by rvslabs on 06/10/15.
 */
public interface GroupDAO {

    public Group getGroup(String jid);
}
