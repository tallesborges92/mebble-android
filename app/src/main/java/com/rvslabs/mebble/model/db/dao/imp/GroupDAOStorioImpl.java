package com.rvslabs.mebble.model.db.dao.imp;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.rvslabs.mebble.model.db.Group;
import com.rvslabs.mebble.model.db.GroupStorIOSQLiteGetResolver;
import com.rvslabs.mebble.model.db.dao.GroupDAO;

import java.util.List;

/**
 * Created by rvslabs on 06/10/15.
 */
public class GroupDAOStorioImpl implements GroupDAO {

    StorIOSQLite storIOSQLite;

    public GroupDAOStorioImpl(@NonNull StorIOSQLite storIOSQLite) {
        this.storIOSQLite = storIOSQLite;

    }

    @Override
    public Group getGroup(String jid) {
        List<Group> groups = storIOSQLite.get().listOfObjects(Group.class).withQuery(
                Query.builder().table("groups").where("jid = ?").whereArgs(jid).limit("1").build())
                .withGetResolver(new GroupStorIOSQLiteGetResolver()).prepare().executeAsBlocking();

        return groups.get(0);
    }


}
