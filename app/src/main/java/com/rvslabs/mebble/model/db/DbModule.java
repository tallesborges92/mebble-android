package com.rvslabs.mebble.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.contentresolver.StorIOContentResolver;
import com.pushtorefresh.storio.contentresolver.impl.DefaultStorIOContentResolver;
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.rvslabs.mebble.model.db.dao.GroupDAO;
import com.rvslabs.mebble.model.db.dao.MessagesDAO;
import com.rvslabs.mebble.model.db.dao.UserDAO;
import com.rvslabs.mebble.model.db.dao.imp.GroupDAOStorioImpl;
import com.rvslabs.mebble.model.db.dao.imp.MessagesDAOStorioImpl;
import com.rvslabs.mebble.model.db.dao.imp.UserDAOStorioImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rvslabs on 11/08/15.
 */
@Module
public class DbModule {

    @Provides
    @NonNull
    @Singleton
    public StorIOSQLite provideStorIOSQLite(@NonNull SQLiteOpenHelper sqLiteOpenHelper) {
        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(sqLiteOpenHelper)
                .addTypeMapping(MessageEntity.class, SQLiteTypeMapping.<MessageEntity>builder()
                        .putResolver(new MessageEntityStorIOSQLitePutResolver())
                        .getResolver(new MessageEntityStorIOSQLiteGetResolver())
                        .deleteResolver(new MessageEntityStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(User.class, SQLiteTypeMapping.<User>builder()
                        .putResolver(new UserStorIOSQLitePutResolver())
                        .getResolver(new UserStorIOSQLiteGetResolver())
                        .deleteResolver(new UserStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(RoomUserEntity.class, SQLiteTypeMapping.<RoomUserEntity>builder()
                        .putResolver(new RoomUserEntityStorIOSQLitePutResolver())
                        .getResolver(new RoomUserEntityStorIOSQLiteGetResolver())
                        .deleteResolver(new RoomUserEntityStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(Group.class, SQLiteTypeMapping.<Group>builder()
                        .putResolver(new GroupStorIOSQLitePutResolver())
                        .getResolver(new GroupStorIOSQLiteGetResolver())
                        .deleteResolver(new GroupStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(ContactUser.class, SQLiteTypeMapping.<ContactUser>builder()
                                .putResolver(new ContactUserStorIOSQLitePutResolver())
                                .getResolver(new ContactUserStorIOSQLiteGetResolver())
                                .deleteResolver(new ContactUserStorIOSQLiteDeleteResolver()).build()
                )
                .build();
    }

    @Provides
    @NonNull
    @Singleton
    public StorIOContentResolver provideStorIOContentResolver(@NonNull Context context) {
        return DefaultStorIOContentResolver.builder()
                .contentResolver(context.getContentResolver())
                .build();
    }

    @Provides
    @NonNull
    @Singleton
    public SQLiteOpenHelper provideSQSqLiteOpenHelper(@NonNull Context context) {
        return new DbOpenHelper(context);
    }

    @Provides
    @NonNull
    @Singleton
    public UserDAO providerUserDAO(@NonNull StorIOSQLite storIOSQLite) {
        return new UserDAOStorioImpl(storIOSQLite);
    }

    @Provides
    @NonNull
    @Singleton
    public GroupDAO providerGroupDAO(@NonNull StorIOSQLite storIOSQLite) {
        return new GroupDAOStorioImpl(storIOSQLite);
    }

    @Provides
    @NonNull
    @Singleton
    public MessagesDAO provideMessagesDAO(@NonNull StorIOSQLite storIOSQLite) {
        return new MessagesDAOStorioImpl(storIOSQLite);
    }

}
