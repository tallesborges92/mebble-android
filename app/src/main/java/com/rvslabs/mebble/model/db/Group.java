package com.rvslabs.mebble.model.db;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.rvslabs.mebble.model.MultipleUsersModel;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.parceler.Parcel;

/**
 * Created by rvslabs on 06/10/15.
 */
@Parcel
@StorIOSQLiteType(table = "groups")
public class Group implements ExtensionElement, MultipleUsersModel {

    @StorIOSQLiteColumn(name = "jid", key = true)
    String jid;
    @StorIOSQLiteColumn(name = "name")
    String name;
    @StorIOSQLiteColumn(name = "image")
    String image;

    public static final String ELEMENT = "x";
    public static final String NAMESPACE = "mebble:group";

    public Group() {
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public CharSequence toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.rightAngleBracket();
        xml.halfOpenElement("group");
        xml.optAttribute("jid", jid);
        xml.optAttribute("name", name);
        xml.optAttribute("image", image);
        xml.closeEmptyElement();
        xml.closeElement(this);
        return xml;
    }

    public String getJID() {
        return jid;
    }

    public void setJID(String jid) {
        this.jid = jid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
