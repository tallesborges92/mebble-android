
package com.rvslabs.mebble.model.api.mebble;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AuthenticationVerifyResponse {

    @Expose
    private String ddi;
    @Expose
    private String id;
    @Expose
    private String number;
    @Expose
    private String password;

    /**
     * 
     * @return
     *     The ddi
     */
    public String getDdi() {
        return ddi;
    }

    /**
     * 
     * @param ddi
     *     The ddi
     */
    public void setDdi(String ddi) {
        this.ddi = ddi;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The number
     */
    public String getNumber() {
        return number;
    }

    /**
     * 
     * @param number
     *     The number
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
