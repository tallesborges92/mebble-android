package com.rvslabs.mebble.model.db.dao;


import com.rvslabs.mebble.model.db.ContactUser;
import com.rvslabs.mebble.model.db.User;

import java.util.List;

/**
 * Created by rvslabs on 23/09/15.
 */
public interface UserDAO {

    User getUser(String userId);

    List<ContactUser> getContactUsers();

    List<ContactUser> getContactUsersByName(String name);
}
