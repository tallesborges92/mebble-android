package com.rvslabs.mebble.model.db;

import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.jivesoftware.smack.packet.Message;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

/**
 * Created by rvslabs on 11/08/15.
 */
@StorIOSQLiteType(table = "messages")
public class MessageEntity {

    @Nullable
    @StorIOSQLiteColumn(name = "_id", key = true)
    Long id;

    @StorIOSQLiteColumn(name = "barejid")
    String bareJid;

    @StorIOSQLiteColumn(name = "fulljid")
    String fullJid;

    @StorIOSQLiteColumn(name = "body")
    String body;

    @StorIOSQLiteColumn(name = "type")
    String type;

    @StorIOSQLiteColumn(name = "userId")
    String userId;

    @StorIOSQLiteColumn(name = "toJid")
    String toJid;

    @StorIOSQLiteColumn(name = "recipientBareJID")
    String recipientBareJID;

    @StorIOSQLiteColumn(name = "component")
    String component;

    @StorIOSQLiteColumn(name = "date")
    Long date;

    @StorIOSQLiteColumn(name = "read")
    Boolean read;


    User userEntity;

    int totalNotReadMessages;

    public MessageEntity() {

    }

    public MessageEntity(Message message) {
        this();

        try {
            Jid jid = JidCreate.from(message.getFrom());
            Jid toJid = JidCreate.from(message.getTo());
            this.bareJid = jid.asBareJidIfPossible().asBareJidString();
            this.fullJid = message.getFrom();
            this.body = message.getBody();
            this.type = message.getType().name();
            this.toJid = toJid.asBareJidIfPossible().asBareJidString();
            this.component = jid.getDomain();
            if (message.getType().equals(Message.Type.groupchat)) {
                this.userId = jid.getResourceOrNull();
            } else {
                this.userId = jid.getLocalpartOrNull();
            }
            this.recipientBareJID = jid.asBareJidIfPossible().asBareJidString();
        } catch (XmppStringprepException ignored) {

        }
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    public String getBareJid() {
        return bareJid;
    }

    public void setBareJid(String bareJid) {
        this.bareJid = bareJid;
    }

    public String getFullJid() {
        return fullJid;
    }

    public void setFullJid(String fullJid) {
        this.fullJid = fullJid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(User userEntity) {
        this.userEntity = userEntity;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToJid() {
        return toJid;
    }

    public String getRecipientBareJID() {
        return recipientBareJID;
    }

    public void setRecipientBareJID(String recipientBareJID) {
        this.recipientBareJID = recipientBareJID;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public void setToJid(String toJid) {
        this.toJid = toJid;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public int getTotalNotReadMessages() {
        return totalNotReadMessages;
    }

    public void setTotalNotReadMessages(int totalNotReadMessages) {
        this.totalNotReadMessages = totalNotReadMessages;
    }
}
