package com.rvslabs.mebble.model.db.dao.imp;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.rvslabs.mebble.model.db.ContactUser;
import com.rvslabs.mebble.model.db.ContactUserStorIOSQLiteGetResolver;
import com.rvslabs.mebble.model.db.User;
import com.rvslabs.mebble.model.db.UserStorIOSQLiteGetResolver;
import com.rvslabs.mebble.model.db.dao.UserDAO;

import java.util.List;

/**
 * Created by rvslabs on 23/09/15.
 */
public class UserDAOStorioImpl implements UserDAO {

    StorIOSQLite storIOSQLite;

    public UserDAOStorioImpl(@NonNull StorIOSQLite storIOSQLite) {
        this.storIOSQLite = storIOSQLite;
    }

    @Override
    public User getUser(String userId) {

        List<User> users = storIOSQLite.get().listOfObjects(User.class).withQuery(
                Query.builder().table("USERS").where("id = ?").whereArgs(userId).limit("1").build())
                .withGetResolver(new UserStorIOSQLiteGetResolver()).prepare().executeAsBlocking();

        return users.get(0);
    }

    @Override
    public List<ContactUser> getContactUsers() {
        List<ContactUser> contactUsers = storIOSQLite.get()
                .listOfObjects(ContactUser.class)
                .withQuery(Query.builder().table("contact_user").build())
                .withGetResolver(new ContactUserStorIOSQLiteGetResolver())
                .prepare()
                .executeAsBlocking();

        return contactUsers;
    }

    @Override
    public List<ContactUser> getContactUsersByName(String name) {
        List<ContactUser> contactUsers = storIOSQLite.get()
                .listOfObjects(ContactUser.class)
                .withQuery(Query.builder().table("contact_user").where("name like ?").whereArgs('%'+name +'%').build())
                .withGetResolver(new ContactUserStorIOSQLiteGetResolver())
                .prepare()
                .executeAsBlocking();

        return contactUsers;
    }


}
