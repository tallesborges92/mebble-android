package com.rvslabs.mebble.model.db;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.jivesoftware.smack.packet.Presence;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

/**
 * Created by rvslabs on 14/08/15.
 */
@StorIOSQLiteType(table = "room_users")
public class RoomUserEntity {

    @StorIOSQLiteColumn(name = "id", key = true)
    String id;

    @StorIOSQLiteColumn(name = "roomId")
    String roomId;

    @StorIOSQLiteColumn(name = "userId")
    String userId;

    User userEntity;

    RoomUserEntity() {

    }

    public String getUserId() {
        return userId;
    }

    public String getId() {
        return id;
    }

    public String getRoomId() {
        return roomId;
    }

    public User getUserEntity() {
        return userEntity;
    }

    public RoomUserEntity(Presence presence) {
        try {
            Jid jid = JidCreate.from(presence.getFrom());

            this.roomId = jid.getLocalpartOrNull();
            this.userId = jid.getResourceOrNull();
            this.id = roomId + "_" + userId;

        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }

    }
}
