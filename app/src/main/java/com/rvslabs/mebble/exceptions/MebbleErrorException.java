package com.rvslabs.mebble.exceptions;

import com.rvslabs.mebble.xmpp.MebbleErrorResult;

/**
 * Created by rvslabs on 25/09/15.
 */
public class MebbleErrorException extends Exception {

    private MebbleErrorResult mebbleErrorResult;

    public MebbleErrorException(MebbleErrorResult mebbleError) {
        this.mebbleErrorResult = mebbleError;
    }

    public MebbleErrorResult getMebbleErrorResult() {
        return mebbleErrorResult;
    }
}
